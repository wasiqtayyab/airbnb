//
//  Utility.swift
//  GrabMyTaxi
//
//  Created by macbook on 20/11/20.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import CoreLocation
//import LanguageManager_iOS
//import PhoneNumberKit

protocol DatePickerDelegate {
    func timePicked(time: String)
}

//import Reachability

let AppUtility =  Utility.sharedUtility()

let ud = UserDefaults.standard

struct Platform {
    static let isSimulator: Bool = {
        var isSim = false
        #if arch(i386) || arch(x86_64)
        isSim = true
        #endif
        return isSim
    }()
}


extension Data {
    
     func getDecodedObject<T>(from object : T.Type)->T? where T : Decodable {
        
        do {
            
            return try JSONDecoder().decode(object, from: self)
            
        } catch let error {
            
            print("Manually parsed  ", (try? JSONSerialization.jsonObject(with: self, options: .allowFragments)) ?? "nil")
            
            print("Error in Decoding OBject ", error)
            return nil
        }
        
    }
    
}


//MARK:- Server URL Definition
let imageDownloadUrl  = ""

//MARK:- Loading Colour
let strloadingColour = "FFCC02"

//MARK:- User Defaults Keys
let strToken = "strToken"
let strAPNSToken = "strAPNSToken"
let strDate = "strDate"
let strURL = "strURL"
let strPicURL = "http://apps.qboxus.com/grocery/"
let InternetConnected = "InternetConnected"

@available(iOS 13.0, *)
let delegate:AppDelegate = UIApplication.shared.delegate as!  AppDelegate



var isPickUp =  false
var statusPickup = "0"
//Pickup
var strLatCurrent  = ""
var strLongCurrent = ""
var currentAddress = ""
//Drop
var strLatDestination  = ""
var strLongDestination = ""
var destinationAddress = ""

var selectMenuIndex = "0"

var numberForPageController = 0

//MARK:- Utility Initialization Methods
class Utility: NSObject{
    
    var percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
    var panGestureRecognizer = UIPanGestureRecognizer()

   // fileprivate let connectivity: Connectivity = Connectivity()
    fileprivate var isCheckingConnectivity: Bool = false
   // let phoneNumberKit = PhoneNumberKit()

    //var fbLogin:FBSDKLoginManager!
    var datePickerDelegate : DatePickerDelegate?
    class func sharedUtility()->Utility!
    {
        struct Static
        {
            static var sharedInstance:Utility?=nil;
            static var onceToken = 0
        }
        Static.sharedInstance = self.init();
        return Static.sharedInstance!
    }
    required override init() {

    }

    //Aniamtion
    func startLoaderAnimation(){

    }
    //MARK:- Get Date Method
    func getDateFromUnixTime(_ unixTime:String) -> String {
        let date = Date(timeIntervalSince1970: Double(unixTime)!)
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        let currentTime =  formatter.string(from: date as Date)
        return currentTime;
    }

    func getAgeYears(birthday:String)-> Int{
        let formatter = DateFormatter()
        formatter.dateFormat = "dd,MMMM yyyy" //"dd-MM-yyyy"
        let birthdate = formatter.date(from: birthday)
        let now = Date()
        let calendar = Calendar.current
        let ageComponents = calendar.dateComponents([.year], from: birthdate!, to: now)
        let age = ageComponents.year!
        return age
    }
    func getAgeFromDOF(date: String) -> (Int,Int,Int) {

        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd"
        let dateOfBirth = dateFormater.date(from: date)

        let calender = Calendar.current

        let dateComponent = calender.dateComponents([.year, .month, .day], from:
                                                        dateOfBirth!, to: Date())

        return (dateComponent.year!, dateComponent.month!, dateComponent.day!)
    }
    func showDatePicker(fromVC : UIViewController){

        let vc = UIViewController()
        vc.preferredContentSize = CGSize(width: 250,height: 250)
        let pickerView = UIDatePicker(frame: CGRect(x: 0, y: 0, width: 250, height: 250))
        pickerView.datePickerMode = .date
        //pickerView.locale = NSLocale(localeIdentifier: "\(Formatter.getInstance.getAppTimeFormat().rawValue)") as Locale
        vc.view.addSubview(pickerView)

        let alertCont = UIAlertController(title: "Choose Date", message: "", preferredStyle: UIAlertController.Style.alert)

        alertCont.setValue(vc, forKey: "contentViewController")
        let setAction = UIAlertAction(title: "Select", style: .default) { (action) in
            if self.datePickerDelegate != nil{

                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy"
                formatter.timeZone = TimeZone.current
                let selectedTime = formatter.string(from: pickerView.date)
                self.datePickerDelegate!.timePicked(time: selectedTime)
            }
        }
        alertCont.addAction(setAction)
        alertCont.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        fromVC.present(alertCont, animated: true)
    }


    //MARK: Check Internet
//    func connected() -> Bool
//    {
//        let reachibility = Reachability.forInternetConnection()
//        let networkStatus = reachibility?.currentReachabilityStatus()
//
//        return networkStatus != NotReachable
//
//    }

    //MARK:- Validation Text
    func hasValidText(_ text:String?) -> Bool
    {
        if let data = text
        {
            let str = data.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            if str.count>0
            {
                return true
            }
            else
            {
                return false
            }
        }
        else
        {
            return false
        }

    }



    //MARK:- Validation Atleast 1 special schracter or number

    func checkTextHaveChracterOrNumber( text : String) -> Bool{

        /* let numberRegEx  = ".*[0-9]+.*"
         let texttest1 = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
         let numberresult = texttest1.evaluate(with: text)
         print("\(numberresult)")*/


        let specialCharacterRegEx  = ".*[!&^%$#@()/_-]+.*"
        let texttest2 = NSPredicate(format:"SELF MATCHES %@", specialCharacterRegEx)

        let specialresult = texttest2.evaluate(with: text)
        print("\(specialresult)")

        //return capitalresult || numberresult || specialresult
        return /*numberresult ||*/ specialresult

    }

    //MARK:- Validation Email
    func isEmail(_ email:String  ) -> Bool
    {
        let strEmailMatchstring = "\\b([a-zA-Z0-9%_.+\\-]+)@([a-zA-Z0-9.\\-]+?\\.[a-zA-Z]{2,6})\\b"
        let regExPredicate = NSPredicate(format: "SELF MATCHES %@", strEmailMatchstring)
        if(!isEmpty(email as String?) && regExPredicate.evaluate(with: email))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //MARK:- Validation Empty
    func isEmpty(_ thing : String? )->Bool {
        if (thing?.count == 0) {
            return true
        }
        return false;
    }

    //MARK:- CNIC Validation
    func isValidIdentityNumber(_ value: String) -> Bool {
        guard
            value.count == 11,
            let digits = value.map({ Int(String($0)) }) as? [Int],
            digits[0] != 0
        else { return false }

        let check1 = (
            (digits[0] + digits[2] + digits[4] + digits[6] + digits[8]) * 7
                - (digits[1] + digits[3] + digits[5] + digits[7])
        ) % 10

        guard check1 == digits[9] else { return false }

        let check2 = (digits[0...8].reduce(0, +) + check1) % 10

        return check2 == digits[10]
    }

    func showLocationPermissionAlert(controller:UIViewController){
        let alertController = UIAlertController(title: "Location Permission Required", message: "Please enable location permissions in settings.", preferredStyle: UIAlertController.Style.alert)

        let okAction = UIAlertAction(title: "Settings", style: .default, handler: {(cAlertAction) in
            //Redirect to Settings app
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        })

        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        alertController.addAction(cancelAction)

        alertController.addAction(okAction)

        controller.present(alertController, animated: true, completion: nil)

        //let customNotification = CustomCRNotification(textColor: .black, backgroundColor: .white, image: UIImage(named: "LogoSignup"))
        //CRNotifications.showNotification(type: customNotification, title: "Location Permission Required".localiz(), message: "Please enable location permissions in settings for better experience".localiz(), dismissDelay: 3)

    }
    //MARK:- Show Alert
    func displayAlert(title titleTxt:String, messageText msg:String, delegate controller:UIViewController) ->()
    {
        let alertController = UIAlertController(title: titleTxt, message: msg, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        controller.present(alertController, animated: true, completion: nil)
        alertController.view.tintColor = UIColor(named:strloadingColour )

    }

   
    
//    //MARK:- Customize Button for mulitple language
//    func CustomizeSimpleButton(btn:UIButton){
//        if LanguageManager.shared.currentLanguage == .ar {
//            btn.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.right
//        }else{
//            btn.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
//        }
//    }
//    //MARK:- Customize Button for mulitple language
//    func CustomizeButton(btn:UIButton){
//        if LanguageManager.shared.currentLanguage == .ar {
//            btn.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.right
//            btn.setImage(UIImage(named:"1-46"), for: .normal)
//        }else{
//            btn.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
//            btn.setImage(UIImage(named:"1-43"), for: .normal)
//        }
//    }
//
//    //MARK:- Customize label for mulitple language
//    func CustomizeLabel(lbl:UILabel){
//        if LanguageManager.shared.currentLanguage == .ar {
//            lbl.semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
//            lbl.textAlignment = .right
//
//        }else{
//            lbl.semanticContentAttribute = UISemanticContentAttribute.forceLeftToRight
//            lbl.textAlignment = .left
//        }
//    }

    //MARK:- Color with HEXA
    func hexStringToUIColor (hex:String,alpa:CGFloat) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(alpa)
        )
    }

    //MARK:- Get & Set Methods For UserDefaults

    func saveObject(obj:String,forKey strKey:String){
        ud.set(obj, forKey: strKey)
    }

    func getObject(forKey strKey:String) -> String {
        if let obj = ud.value(forKey: strKey) as? String{
            let obj2 = ud.value(forKey: strKey) as! String
            return obj2
        }else{
            return ""
        }
    }

    func deleteObject(forKey strKey:String) {
        ud.set(nil, forKey: strKey)
    }


    //MARK: Add Delay
    func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }

    //MARK: Calculate Time

    func timeAgoSinceDate(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
       
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())

        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }

    }

    func calculateTimeDifference(start: String) -> String {
        let formatter = DateFormatter()
        //        2018-12-17 18:01:34
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        var startString = "\(start)"
        if startString.count < 4 {
            for _ in 0..<(4 - startString.count) {
                startString = "0" + startString
            }
        }
        let currentDateTime = Date()
        let strcurrentDateTime = formatter.string(from: currentDateTime)
        var endString = "\(strcurrentDateTime)"
        if endString.count < 4 {
            for _ in 0..<(4 - endString.count) {
                endString = "0" + endString
            }
        }
        let startDate = formatter.date(from: startString)!
        let endDate = formatter.date(from: endString)!
        let difference = endDate.timeIntervalSince(startDate)
        if (difference / 3600) > 24{
            let differenceInDays = Int(difference/(60 * 60 * 24 ))
            return "\(differenceInDays) DAY AGO"
        }else{
            return "\(Int(difference) / 3600)HOURS \(Int(difference) % 3600 / 60)MIN AGO"
        }
    }

    func offsetFrom(dateFrom : Date,dateTo:Date) -> String {

        let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: dateFrom, to: dateTo);

        let seconds = "\(difference.second ?? 0)s"
        let minutes = "\(difference.minute ?? 0)m" + ":" + seconds
        let hours = "\(difference.hour ?? 0)h" + ":" + minutes
        let days = "\(difference.day ?? 0)d" + ":" + hours

        if let day = difference.day, day          > 0 { return days }
        if let hour = difference.hour, hour       > 0 { return hours }
        if let minute = difference.minute, minute > 0 { return minutes }
        if let second = difference.second, second > 0 { return seconds }
        return ""
    }

    //MARK: Random Number
    func getCurrentMillis()->Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    //MARK: Send Notification


   /* //MARK: Location
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        if !self.connected(){
            return
        }
        //var addressString : String = ""
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon

        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)

        ceo.reverseGeocodeLocation(loc, completionHandler:
                                    {(placemarks, error) in
                                        if (error != nil)
                                        {
                                            print("reverse geodcode fail: \(error!.localizedDescription)")
                                        }
                                        let pm = placemarks! as [CLPlacemark]

                                        if pm.count > 0 {
                                            let pm = placemarks![0]
                                            print(pm.country)
                                            print(pm.locality)
                                            print(pm.subLocality)
                                            print(pm.thoroughfare)
                                            print(pm.postalCode)
                                            print(pm.subThoroughfare)
                                            if pm.locality != nil {
                                                //addressString = addressString + pm.subLocality! + ", "
                                                //User have option to chnage his city
                                                //strCurrentCityName = pm.locality!
                                            }
                                            if pm.country != nil {
                                                //  strCurrentCountryName = pm.country!
                                            }
                                            NotificationCenter.default.post(name: Notification.Name("LocationGeoCoding"), object: nil)
                                            /*if pm.thoroughfare != nil {
                                             addressString = addressString + pm.thoroughfare! + ", "
                                             }
                                             if pm.locality != nil {
                                             addressString = addressString + pm.locality! + ", "
                                             }
                                             if pm.country != nil {
                                             addressString = addressString + pm.country! + ", "
                                             }
                                             if pm.postalCode != nil {
                                             addressString = addressString + pm.postalCode! + " "
                                             }*/
                                            //print(addressString)
                                        }
                                    })
    }
    internal func createActivityIndicator(_ uiView : UIView)->UIView{

        let container: UIView = UIView(frame: CGRect.zero)
        container.layer.frame.size = uiView.frame.size
        container.center = CGPoint(x: uiView.bounds.width/2, y: uiView.bounds.height/2)
        container.backgroundColor = #colorLiteral(red: 0.9217679501, green: 0.9217679501, blue: 0.9217679501, alpha: 0.4444028253) //UIColor(white: 0.2, alpha: 0.3)

        let loadingView: UIView = UIView()
        loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        loadingView.center = container.center
        loadingView.backgroundColor = UIColor.clear  // UIColor(white:0.1, alpha: 0.7)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        loadingView.layer.shadowRadius = 5
        loadingView.layer.shadowOffset = CGSize(width: 0, height: 4)
        loadingView.layer.opacity = 2
        loadingView.layer.masksToBounds = false
        // loadingView.layer.shadowColor = .clear

        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        actInd.clipsToBounds = true
        actInd.color = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        actInd.style = .whiteLarge

        actInd.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
        loadingView.addSubview(actInd)
        container.addSubview(loadingView)
        container.isHidden = true
        uiView.addSubview(container)
        actInd.startAnimating()

        return container

    }

    //    MARK:- add device data
//    func addDeviceData(){
//
//        let uid = UserDefaults.standard.string(forKey: "userID")
//        let fcm = UserDefaults.standard.string(forKey: "DeviceToken")
//        let ip = getIPAddress()
//        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
//        let deviceType = "iOS"
//
//
//        ApiHandler.sharedInstance.addDeviceData(user_id: uid!, device: deviceType, version: appVersion!, ip: ip, device_token: fcm!) { (isSuccess, response) in
//            if isSuccess{
//
//                if response?.value(forKey: "code") as! NSNumber == 200{
//                    print("Data of Device Added: ",response?.value(forKey: "msg"))
//                }else{
//                    print("!200: ",response?.value(forKey: "msg"))
//                }
//
//            }
//        }
//    }
    func getIPAddress() -> String {
        var address: String?
        var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            var ptr = ifaddr
            while ptr != nil {
                defer { ptr = ptr?.pointee.ifa_next }

                guard let interface = ptr?.pointee else { return "" }
                let addrFamily = interface.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {

                    // wifi = ["en0"]
                    // wired = ["en2", "en3", "en4"]
                    // cellular = ["pdp_ip0","pdp_ip1","pdp_ip2","pdp_ip3"]

                    let name: String = String(cString: (interface.ifa_name))
                    if  name == "en0" || name == "en2" || name == "en3" || name == "en4" || name == "pdp_ip0" || name == "pdp_ip1" || name == "pdp_ip2" || name == "pdp_ip3" {
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        getnameinfo(interface.ifa_addr, socklen_t((interface.ifa_addr.pointee.sa_len)), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                        address = String(cString: hostname)
                    }
                }
            }
            freeifaddrs(ifaddr)
        }
        return address ?? ""
    }*/

//    //MARK:- Phone Number Validation
//    func isValidPhoneNumber(strPhone:String) -> Bool{
//        do {
//            _ = try phoneNumberKit.parse(strPhone)
//            return true
//        }
//        catch {
//            print("Generic parser error")
//            return false
//        }
//    }

    //    MARK:- validate username
    func validateUsername(str: String) -> Bool
    {
        let RegEx = "\\w{4,14}"
        let Test = NSPredicate(format:"SELF MATCHES %@", RegEx)
        return Test.evaluate(with: str)
    }
    
    
    

}

extension UIViewController: UINavigationControllerDelegate {
    
    
    func addNewGesture() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            return
        }
        
        AppUtility!.panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.newhandlePanGesture(_:)))
        self.view.addGestureRecognizer(AppUtility!.panGestureRecognizer)
        
        
    }
    
    @objc  func newhandlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
        
        case .began:
            navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            if AppUtility!.percentDrivenInteractiveTransition != nil {
                AppUtility!.percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
             AppUtility!.percentDrivenInteractiveTransition.finish()
                
            } else {
                AppUtility!.percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
           AppUtility!.percentDrivenInteractiveTransition.cancel()
            break
        default:
            break
        }
    }
    
    
    
//    public func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        
//        return SlideAnimatedTransitioning()
//    }
    
    public func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if AppUtility!.panGestureRecognizer.state == .began {
            AppUtility!.percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            AppUtility!.percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
          //  AppUtility!.percentDrivenInteractiveTransition = nil
        }
        
        return AppUtility!.percentDrivenInteractiveTransition
    }
}


//class Loader{
//
//    var vc = UIApplication.shared.keyWindow?.rootViewController?.view
//
//    func activityStartAnimating() {
//
//        if var topController = UIApplication.shared.keyWindow?.rootViewController {
//            while let presentedViewController = topController.presentedViewController {
//                topController = presentedViewController
//                print("topController: ",topController)
//
//                vc = topController.view
//            }
//        }
//
//        vc?.isUserInteractionEnabled = false
//
//        AppUtility!.checkInternetConnection()
//
//        let backgroundView = UIView()
//        backgroundView.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
//        backgroundView.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 0.6)
//        backgroundView.tag = 475647
//
//        var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
//        activityIndicator = UIActivityIndicatorView(frame: CGRect.init(x: 0, y: 0, width: 80, height: 80))
//        activityIndicator.center = vc!.center
//        activityIndicator.hidesWhenStopped = true
//        activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
//        activityIndicator.color = .white
//        activityIndicator.startAnimating()
//        vc!.isUserInteractionEnabled = false
//
//        backgroundView.addSubview(activityIndicator)
//
//        vc!.addSubview(backgroundView)
//    }
//
//    func activityStopAnimating() {
//
//        vc?.isUserInteractionEnabled = true
//
//        AppUtility!.stopConnectivityChecks()
//
//        if let background = vc!.viewWithTag(475647){
//            background.removeFromSuperview()
//        }
//        vc!.isUserInteractionEnabled = true
//    }
//
//
//}

//MARK:- Internet connection check setup
//extension Utility{
//
//    func checkInternetConnection(){
//        if #available(iOS 12, *) {
//
//            connectivity.framework = .network
//        }
//        else {
//
//            connectivity.framework = .systemConfiguration
//        }
//
//        startConnectivityChecks()
//        performSingleConnectivityCheck()
//        configureConnectivityNotifier()
//
//    }
//
//    func configureConnectivityNotifier() {
//        let connectivityChanged: (Connectivity) -> Void = { [weak self] connectivity in
//            self?.updateConnectionStatus(connectivity.status)
//        }
//        connectivity.whenConnected = connectivityChanged
//        connectivity.whenDisconnected = connectivityChanged
//    }
//
//    func performSingleConnectivityCheck() {
//        connectivity.checkConnectivity { connectivity in
//            self.updateConnectionStatus(connectivity.status)
//        }
//    }
//
//    func startConnectivityChecks() {
////        activityIndicator.startAnimating()
//        connectivity.startNotifier()
//        isCheckingConnectivity = true
////        segmentedControl.isEnabled = false
//        updateNotifierButton(isCheckingConnectivity: isCheckingConnectivity)
//    }
//
//    func stopConnectivityChecks() {
////        activityIndicator.stopAnimating()
//        connectivity.stopNotifier()
//        isCheckingConnectivity = false
////        segmentedControl.isEnabled = true
//        updateNotifierButton(isCheckingConnectivity: isCheckingConnectivity)
//    }
//
////    func updateConnectionStatus(_ status: Connectivity.Status) {
////
////        let Alert = alert()
////        switch status {
////        case .connectedViaWiFi, .connectedViaCellular, .connected:
////            //            statusLabel.textColor = UIColor.darkGreen
////            stopConnectivityChecks()
////
////            break
////        case .connectedViaWiFiWithoutInternet, .connectedViaCellularWithoutInternet, .notConnected:
////            //            statusLabel.textColor = UIColor.red
////            Alert.msg(message: status.description, title: "Check Internet")
////                    break
////        case .determining:
////
////            Alert.msg(message: status.description, title: "Check Internet")
////                    break
////        //            statusLabel.textColor = UIColor.black
////        }
////
////        print("internet status",status.description)
////    }
//
//    func updateNotifierButton(isCheckingConnectivity: Bool) {
//        let buttonText = isCheckingConnectivity ? "Stop notifier" : "Start notifier"
////        let buttonTextColor = isCheckingConnectivity ? UIColor.red : UIColor.darkGreen
////        notifierButton.setTitle(buttonText, for: .normal)
////        notifierButton.setTitleColor(buttonTextColor, for: .normal)
//    }
//
//
//
//}
//class alert {
//
//    func msg(message: String, title: String = "")
//    {
//        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
//
//        alertView.addAction(UIAlertAction(title: "OK", style: .destructive, handler: { (alert) in
//            Utility.sharedUtility()?.stopConnectivityChecks()
//            Utility.sharedUtility()?.connectivity.stopNotifier()
//        }))
//
//
//        if var topController = UIApplication.shared.keyWindow?.rootViewController {
//            while let presentedViewController = topController.presentedViewController {
//                topController = presentedViewController
//                print("topController: ",topController)
//                DispatchQueue.main.async {
//                    topController.present(alertView, animated: true, completion: nil)
////                    presenting the alert here
//                }
//
//            }
//        }
//
//    }
//}
class Loader{
    
    var vc = UIApplication.shared.keyWindow?.rootViewController?.view
    
    
    let backgroundView = UIView()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    func activityStartAnimating() {
        
        vc!.isUserInteractionEnabled = false
        backgroundView.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        backgroundView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.8112960188)
        backgroundView.tag = 475647
        
        
        activityIndicator = UIActivityIndicatorView(frame: CGRect.init(x: 0, y: 0, width: 80, height: 80))
        activityIndicator.tag = 475648
        activityIndicator.center = backgroundView.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
        activityIndicator.color = .black
        activityIndicator.startAnimating()
        
        
        backgroundView.addSubview(activityIndicator)
        
        DispatchQueue.main.async { [self] in
            UIApplication.shared.keyWindow!.addSubview(backgroundView)
            UIApplication.shared.keyWindow!.bringSubviewToFront(backgroundView)
            self.backgroundView.bringSubviewToFront(backgroundView)
        }
        
    }
    
    func activityStopAnimating() {

        DispatchQueue.main.async {
            if self.backgroundView.viewWithTag(475647) != nil{
                self.backgroundView.removeFromSuperview()
            }
            
            if self.activityIndicator.viewWithTag(475648) != nil{
                self.activityIndicator.removeFromSuperview()
            }
        }
        
        vc?.isUserInteractionEnabled = true
        
    }
    
    
}
