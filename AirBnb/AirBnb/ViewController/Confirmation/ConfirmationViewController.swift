//
//  ConfirmationViewController.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 07/05/2021.
//

import UIKit

class ConfirmationViewController: UIViewController {

    //MARK:- OUTLET
    
    
    @IBOutlet weak var lblEmail: UILabel!
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblEmail.text = VariableDecleration.shared.email

        
    }
    
    //MARK:- FUNCTION
    
    
    
    
    //MARK:- BUTTON ACTION
    
    
    @IBAction func startButtonPressed(_ sender: UIButton){
//        let vc = storyboard?.instantiateViewController(withIdentifier: "")as!
//        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
    
        self.navigationController?.popViewController(animated: true)
    }
    
    

}
