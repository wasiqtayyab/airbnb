//
//  PastBookingsViewController.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 01/05/2021.
//

import UIKit
import Cosmos
class PastBookingsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
   
    
    
    //MARK:- OUTLET
    
    
    @IBOutlet weak var tableView1: UITableView!
    
    
   
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView1.dataSource = self
        tableView1.delegate = self
        
        
    }
    

    //MARK:- TABLE VIEW
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PastBookingsTableViewCell", for: indexPath)as! PastBookingsTableViewCell
        
    
      
        cell.roomView.layer.shadowColor = #colorLiteral(red: 0.8862745098, green: 0.8862745098, blue: 0.8862745098, alpha: 1)
        cell.roomView.layer.shadowOpacity = 1
        cell.roomView.layer.shadowOffset = .zero
        cell.roomView.layer.shadowRadius = 10
        
        cell.roomImage.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 270
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "BookingDetailViewController")as! BookingDetailViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
