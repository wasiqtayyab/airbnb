//
//  PastBookingsTableViewCell.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 01/05/2021.
//

import UIKit
import Cosmos
class PastBookingsTableViewCell: UITableViewCell {
    //MARK:- OUTLET
    
    @IBOutlet weak var roomView: UIView!
    @IBOutlet weak var lblRupee: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var roomImage: UIImageView!
    @IBOutlet weak var cosmoView: CosmosView!
    @IBOutlet weak var lblRating: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
