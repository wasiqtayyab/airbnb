//
//  BookingDetailViewController.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 23/05/2021.
//

import UIKit

class BookingDetailViewController: UIViewController {
    
    //MARK:- OUTLET
    @IBOutlet weak var roomView: UIView!
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        addShadow()
        
    }
    
    //MARK:- FUNCTION
    func addShadow(){
        
        roomView.layer.shadowColor = #colorLiteral(red: 0.9215686275, green: 0.9215686275, blue: 0.9215686275, alpha: 1)
        roomView.layer.shadowOpacity = 1
        roomView.layer.shadowOffset = .zero
        roomView.layer.shadowRadius = 10
    
    }
    
    //MARK:- BUTTON ACTION
    
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
   

}
