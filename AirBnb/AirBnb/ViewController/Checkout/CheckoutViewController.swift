//
//  CheckoutViewController.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 18/05/2021.
//

import UIKit

class CheckoutViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var lblCheckIn: UILabel!
    @IBOutlet weak var roomView: UIView!
    @IBOutlet weak var dateinView: UIView!
    
    @IBOutlet weak var guestView: UIView!
    @IBOutlet weak var dateoutView: UIView!
    @IBOutlet weak var lblCheckOut: UILabel!
    @IBOutlet weak var lblGuest: UILabel!
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        addShadow()
        addBorder()
    }
    
    //MARK:- FUNCTION
    func addShadow(){
        
        roomView.layer.shadowColor = #colorLiteral(red: 0.9176470588, green: 0.9176470588, blue: 0.9176470588, alpha: 1)
        roomView.layer.shadowOpacity = 1
        roomView.layer.shadowOffset = .zero
        roomView.layer.shadowRadius = 10
    
    }
    
    func addBorder() {
        
        dateinView.layer.borderWidth = 1
        dateinView.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        
        dateoutView.layer.borderWidth = 1
        dateoutView.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        
        guestView.layer.borderWidth = 1
        guestView.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        
       
        
    }
    
    //MARK:- BUTTON ACTION
    
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
    }
    

}
