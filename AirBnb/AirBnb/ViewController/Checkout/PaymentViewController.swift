//
//  PaymentViewController.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 18/05/2021.
//

import UIKit

class PaymentViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var imgTickMassage: UIImageView!
    @IBOutlet weak var btnCheckedMassage: UIButton!
    @IBOutlet weak var btnMassage: UIButton!
    
    @IBOutlet weak var btnPickup: UIButton!
    @IBOutlet weak var imgTickPickup: UIImageView!
    @IBOutlet weak var btnCheckedPickup: UIButton!
    
    @IBOutlet weak var imgTickBreakfast: UIImageView!
    @IBOutlet weak var btnCheckedBreakfast: UIButton!
    @IBOutlet weak var btnBreakfast: UIButton!
    
    @IBOutlet weak var btnCreditCard: UIButton!
    @IBOutlet weak var imgCreditCard: UIImageView!
    
    @IBOutlet weak var cardnumberView: UIView!
    
    @IBOutlet weak var expirationView: UIView!
    @IBOutlet weak var cvvView: UIView!
    
    //MARK:- DECLERATION
    
    var Selected = false
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnCheckedMassage.isHidden = true
        btnCheckedPickup.isHidden = true
        btnCheckedBreakfast.isHidden = true
        addBorder()
    }
    
    
    //MARK:- FUNCTION
    
    
    func addBorder() {
        
        btnMassage.layer.borderWidth = 1
        btnMassage.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        
        expirationView.layer.borderWidth = 1
        expirationView.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        
        cvvView.layer.borderWidth = 1
        cvvView.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        
        cardnumberView.layer.borderWidth = 1
        cardnumberView.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        
        btnPickup.layer.borderWidth = 1
        btnPickup.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        
        btnBreakfast.layer.borderWidth = 1
        btnBreakfast.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        
        btnCreditCard.layer.borderWidth = 1
        btnCreditCard.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func massageButtonPressed(_ sender: UIButton) {
        
        if Selected == false {
            imgTickMassage.image = UIImage(named: "Rectangle 2-1")
            btnCheckedMassage.isHidden = false
            btnCheckedMassage.setImage(UIImage(named: "ic_tick"), for: .normal)
            Selected = true
        }else {
            
            btnCheckedMassage.isHidden = true
            imgTickMassage.image = UIImage(named: "Rectangle")
            Selected = false
            
        }
        
        
    }
    
    @IBAction func airportButtonPressed(_ sender: UIButton) {
        
        if Selected == false {
            imgTickPickup.image = UIImage(named: "Rectangle 2-1")
            btnCheckedPickup.isHidden = false
            btnCheckedPickup.setImage(UIImage(named: "ic_tick"), for: .normal)
            Selected = true
        }else {
            
            btnCheckedPickup.isHidden = true
            imgTickPickup.image = UIImage(named: "Rectangle")
            Selected = false
            
        }
    }
    
    @IBAction func breakfastButtonPressed(_ sender: UIButton) {
        
        if Selected == false {
            imgTickBreakfast.image = UIImage(named: "Rectangle 2-1")
            btnCheckedBreakfast.isHidden = false
            btnCheckedBreakfast.setImage(UIImage(named: "ic_tick"), for: .normal)
            Selected = true
        }else {
            
            btnCheckedBreakfast.isHidden = true
            imgTickBreakfast.image = UIImage(named: "Rectangle")
            Selected = false
            
        }
    }
    
    
    @IBAction func creditcardButtonPressed(_ sender: UIButton) {
    }
    
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
    }
    
}
