//
//  PropertyFilterViewController.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 29/04/2021.
//

import UIKit

class PropertyFilterViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    //MARK:- OUTLET
    
    
    @IBOutlet weak var filterSegment: UISegmentedControl!
    
    @IBOutlet weak var tableView1: UITableView!
    @IBOutlet weak var filterView: UIView!
    
    //MARK:- DECLERATION
    
    
    
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CustomizationSegment()
        Setup()
        tableView1.delegate = self
        tableView1.dataSource = self
    }
    
    //MARK:- FUNCTION
    
    func CustomizationSegment(){
        filterSegment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: UIControl.State.selected)
        
        let font = UIFont.init(name: "SofiaPro", size: 14.0)
        filterSegment.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
        
    }
    
    func Setup(){
        
        filterView.layer.cornerRadius = 15
        filterView.layer.shadowColor = #colorLiteral(red: 0.8078431373, green: 0.8078431373, blue: 0.8078431373, alpha: 1)
        filterView.layer.shadowOpacity = 1
        filterView.layer.shadowOffset = .zero
        filterView.layer.shadowRadius = 10
        
        
        
    }
    
    
    //MARK:- BUTTON ACTION
    
    @IBAction func filterValueChanged(_ sender: UISegmentedControl) {
        
        self.tableView1.reloadData()
        
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- TABLEVIEW
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PropertyFilterTableViewCell", for: indexPath)as! PropertyFilterTableViewCell
        let selectedIndex = self.filterSegment.selectedSegmentIndex
        print(selectedIndex)
        
        
        if selectedIndex == 0 {
            
            cell.lblAmenities.text = "Guesthouse"
            cell.lblProperty.text = "(0 property)"
            cell.imgSelected.image = UIImage(named: "Oval")
            return cell
        }else if selectedIndex == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PropertyFilterTableViewCell", for: indexPath)as! PropertyFilterTableViewCell
            cell.lblAmenities.text = "Wifi"
            cell.lblProperty.text = "(4 properties)"
            cell.imgSelected.image = UIImage(named: "Rectangle")
            return cell
            
        }
        return cell
       
    }
    
    
    
}
