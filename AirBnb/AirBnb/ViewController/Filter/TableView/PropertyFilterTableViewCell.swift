//
//  PropertyFilterTableViewCell.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 29/04/2021.
//

import UIKit

class PropertyFilterTableViewCell: UITableViewCell {
    //MARK:- OUTLET
    
    @IBOutlet weak var lblProperty: UILabel!
    @IBOutlet weak var lblAmenities: UILabel!
    @IBOutlet weak var imgSelected: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
