//
//  OtpCodeViewController.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 06/05/2021.
//

import UIKit
import CHIOTPField
class OtpCodeViewController: UIViewController {

    //MARK:- OUTLET

    @IBOutlet weak var lblNumber: UILabel!
    
    @IBOutlet weak var tfOTP: UITextField!
    
    //MARK:- VARIABLE
    
    var phoneCell: CHIOTPFieldTwo {

    return tfOTP as! CHIOTPFieldTwo

    }
    
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        lblNumber.text = VariableDecleration.shared.phone_Number
       
    }
    
    func checkValidity(){
        
        if AppUtility!.isEmpty(self.tfOTP.text!){
                   AppUtility?.displayAlert(title: NSLocalizedString("Error", comment: ""), messageText: NSLocalizedString("Enter a valid code", comment: ""), delegate: self)
                   return
               }
    
       
    }
    
    //MARK:- BUTTON ACTION
    
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    
    }
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        print(phoneCell.text!)
        checkValidity()
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "InformationViewController")as! InformationViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
