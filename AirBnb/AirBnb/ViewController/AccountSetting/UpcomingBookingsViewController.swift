//
//  UpcomingBookingsViewController.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 07/06/2021.
//

import UIKit

class UpcomingBookingsViewController: UIViewController {

    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
