//
//  UpdateCredentialsViewController.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 07/06/2021.
//

import UIKit

class UpdateCredentialsViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var oldPasswordView: UIView!
    @IBOutlet weak var newPasswordView: UIView!
    @IBOutlet weak var confirmPasswordView: UIView!
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()

       addBorder()
    }
    
    //MARK:- FUNCTION
    
    func addBorder() {
        
        oldPasswordView.layer.borderWidth = 1
        oldPasswordView.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        
        newPasswordView.layer.borderWidth = 1
        newPasswordView.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        
        confirmPasswordView.layer.borderWidth = 1
        confirmPasswordView.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        
        
    }
    
    //MARK:- BUTTON ACTION
    

    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
