//
//  EditProfileViewController.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 08/06/2021.
//

import UIKit

class EditProfileViewController: UIViewController {
    //MARK:- OUTLET
    
    @IBOutlet weak var firstNameView: UIView!
    @IBOutlet weak var lastNameView: UIView!
    @IBOutlet weak var emailAddressView: UIView!
    
    @IBOutlet weak var genderView: UIView!
    @IBOutlet weak var dobView: UIView!
    @IBOutlet weak var phoneNumberView: UIView!
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addBorder()
        
    }
    //MARK:- FUNCTION
    func addBorder() {
        
        firstNameView.layer.borderWidth = 1
        firstNameView.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        
        lastNameView.layer.borderWidth = 1
        lastNameView.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        
        emailAddressView.layer.borderWidth = 1
        emailAddressView.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        
        genderView.layer.borderWidth = 1
        genderView.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        
        dobView.layer.borderWidth = 1
        dobView.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        
        phoneNumberView.layer.borderWidth = 1
        phoneNumberView.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        
        
        
    }
    
    
    
    
    //MARK:- BUTTON ACTION
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
