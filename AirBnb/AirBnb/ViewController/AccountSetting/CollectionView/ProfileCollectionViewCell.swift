//
//  ProfileCollectionViewCell.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 01/05/2021.
//

import UIKit

class ProfileCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var lblProfile: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
}
