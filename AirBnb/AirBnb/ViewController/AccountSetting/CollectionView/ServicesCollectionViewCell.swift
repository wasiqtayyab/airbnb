//
//  ServicesCollectionViewCell.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 01/05/2021.
//

import UIKit

class ServicesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgService: UIImageView!
    
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var lblService: UILabel!
}
