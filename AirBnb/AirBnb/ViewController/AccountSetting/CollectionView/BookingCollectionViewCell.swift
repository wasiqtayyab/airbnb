//
//  BookingCollectionViewCell.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 01/05/2021.
//

import UIKit

class BookingCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bookingImage: UIImageView!
    
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var bookingLabel: UILabel!
}
