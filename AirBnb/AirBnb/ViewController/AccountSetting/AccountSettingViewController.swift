//
//  AccountSettingViewController.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 30/04/2021.
//

import UIKit
import KWDrawerController
class AccountSettingViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate  {
    
    
    //MARK:- OUTLET
    
    @IBOutlet weak var btnSetting: UIButton!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var collectionView1: UICollectionView!
    @IBOutlet weak var collectionView2: UICollectionView!
    @IBOutlet weak var collectionView3: UICollectionView!
    
    @IBOutlet weak var userView: UIView!
    
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var servicesView: UIView!
    
    @IBOutlet weak var bookingView: UIView!
    
    //MARK:- DECLERATION
    
    let proArr = [["Label": "Profile", "Image" : "profile"],
                  ["Label": "Security", "Image" : "password"]]
    let bookArr = [["Label": "History", "Image" : "History"],
                   ["Label": "Upcoming", "Image" : "upcoming"]]
    let serviceArr = [["Label": "Login", "Image" : "lock"],
                   ["Label": "Notification", "Image" : "notification"],
                   ["Label": "My Reviews", "Image" : "star"]]
    
    
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView1.delegate = self
        collectionView1.dataSource = self
        collectionView2.delegate = self
        collectionView2.dataSource = self
        collectionView3.delegate = self
        collectionView3.dataSource = self
        addShadow()
    }
    
    
    //MARK:- FUNCTION
    
    func addShadow(){
        
        userView.layer.shadowColor = #colorLiteral(red: 0.9568627451, green: 0.9647058824, blue: 0.9725490196, alpha: 1)
        userView.layer.shadowOpacity = 1
        userView.layer.shadowOffset = .zero
        userView.layer.shadowRadius = 10
        
        profileView.layer.shadowColor = #colorLiteral(red: 0.9568627451, green: 0.9647058824, blue: 0.9725490196, alpha: 1)
        profileView.layer.shadowOpacity = 1
        profileView.layer.shadowOffset = .zero
        profileView.layer.shadowRadius = 10
        
        servicesView.layer.shadowColor = #colorLiteral(red: 0.9568627451, green: 0.9647058824, blue: 0.9725490196, alpha: 1)
        servicesView.layer.shadowOpacity = 1
        servicesView.layer.shadowOffset = .zero
        servicesView.layer.shadowRadius = 10
        
        bookingView.layer.shadowColor = #colorLiteral(red: 0.9568627451, green: 0.9647058824, blue: 0.9725490196, alpha: 1)
        bookingView.layer.shadowOpacity = 1
        bookingView.layer.shadowOffset = .zero
        bookingView.layer.shadowRadius = 10
        
        imgUser.layer.borderColor = #colorLiteral(red: 0.8680242896, green: 0.41815117, blue: 0.3688527942, alpha: 1)
        imgUser.layer.borderWidth = 0.5
        
        
    }
    
    
    
    
    //MARK:- COLLECTION VIEW
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionView1 {
            return proArr.count
        }else if collectionView == collectionView2 {
            return bookArr.count
        }else {
            return serviceArr.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionView1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileCollectionViewCell", for: indexPath)as! ProfileCollectionViewCell
            cell.lblProfile.text = proArr[indexPath.row]["Label"]
            cell.profileImage.image = UIImage(named: proArr[indexPath.row]["Image"]!)
            
            if indexPath.row == 1{
                cell.lineView.isHidden = true
            }
            return cell
            
        }else if collectionView == collectionView2 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookingCollectionViewCell", for: indexPath)as! BookingCollectionViewCell
            cell.bookingLabel.text = bookArr[indexPath.row]["Label"]
            cell.bookingImage.image = UIImage(named: bookArr[indexPath.row]["Image"]!)
            
            if indexPath.row == 1{
                cell.lineView.isHidden = true
            }
            return cell
            
        }else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ServicesCollectionViewCell", for: indexPath)as! ServicesCollectionViewCell
            cell.lblService.text = serviceArr[indexPath.row]["Label"]
            cell.imgService.image = UIImage(named: serviceArr[indexPath.row]["Image"]!)
            
            if indexPath.row == 2{
                cell.lineView.isHidden = true
            }
            return cell
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
     if collectionView == collectionView1{
        return CGSize(width: collectionView.frame.width / 4 , height: 77)
     }else
     if collectionView == collectionView2{
       return CGSize(width: collectionView.frame.width / 4 , height: 77)
     }else
     if collectionView == collectionView3{
         return CGSize(width: collectionView.frame.width / 4 , height: 77)
     }else {
        return CGSize(width: collectionView.frame.width / 4 , height: 77)
     }
  }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collectionView1{
            if indexPath.row == 0{
               
                let vc = storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController")as!
                    EditProfileViewController
                self.navigationController?.pushViewController(vc, animated: true)
                
                
            }else
            if indexPath.row == 1{
                let vc = storyboard?.instantiateViewController(withIdentifier: "ManageSessionsViewController")as!
                    ManageSessionsViewController
                self.navigationController?.pushViewController(vc, animated: true)
                
               
            }
            
            
        }else if collectionView == collectionView2{
            
            if indexPath.row == 0{
                let vc = storyboard?.instantiateViewController(withIdentifier: "PastBookingsViewController")as! PastBookingsViewController
                self.navigationController?.pushViewController(vc, animated: true)
                
                
            }else if indexPath.row == 1{
                
                let vc = storyboard?.instantiateViewController(withIdentifier: "UpcomingBookingsViewController")as! UpcomingBookingsViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
            
        }else if collectionView == collectionView3{
            
            if indexPath.row == 0{
                let vc = storyboard?.instantiateViewController(withIdentifier: "UpdateCredentialsViewController")as!
                    UpdateCredentialsViewController
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else
            if indexPath.row == 1{
                let vc = storyboard?.instantiateViewController(withIdentifier: "NotificationViewController")as! NotificationViewController
                self.navigationController?.pushViewController(vc, animated: true)
                
                
            }else if indexPath.row == 2{
                let vc = storyboard?.instantiateViewController(withIdentifier: "AllReviewsViewController")as! AllReviewsViewController
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
        }
    }
    
    //MARK:- IMAGE PICKER DELEGATE
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        imgUser.image = image
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    //MARK:- BUTTON ACTION
    
    @IBAction func menuButtonPressed(_ sender: UIButton) {
        
        self.drawerController?.setAbsolute(true, for: .left)
        self.drawerController?.openSide(.left)
    }
    
    
    @IBAction func updateButtonPressed(_ sender: UIButton) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController()
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action: UIAlertAction) in
            imagePickerController.sourceType = .camera
            self.present(imagePickerController,animated: true,completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in
            self.present(imagePickerController,animated: true,completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            
        self.present(actionSheet,animated: true,completion: nil)
       
    }
    
    @IBAction func settingButtonPressed(_ sender: UIButton) {
    }
    
}
