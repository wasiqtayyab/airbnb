//
//  ViewController.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 23/04/2021.
//

import UIKit
import KWDrawerController

class MenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    //MARK:- OUTLET
    
    @IBOutlet weak var tableView1: UITableView!
    
    
    @IBOutlet weak var tblConstr: NSLayoutConstraint!
    
    
    //MARK:- ARRAY DECLARATION
    
    var seleectMenuIndex = "0"
    var arrMenu = [["name":"Discover","image":"ic_discovery_outline_black"],
                   ["name":"Account","image":"ic_profile_outline_black"],
                   ["name":"Privacy","image":"ic_document_outline_black"],
                   ["name":"Rate App","image":"ic_star_outline_black"],
                   ["name":"Share App","image":"ic_share_outline_black"],
                   ["name":"Login","image":"ic_logout_outline_black"]]
    
    
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView1.delegate = self
        tableView1.dataSource = self
        self.tableView1.tableFooterView = UIView()
        tblConstr.constant = CGFloat((arrMenu.count * 50))
        
    }
    
    
    //MARK:- KWDrawController
    
    func drawController(id:String){
        
        let vc = storyboard?.instantiateViewController(withIdentifier:id)
        self.drawerController?.setViewController(vc, for: .none)
        self.drawerController?.closeSide()
        print("@inde1 ",self.drawerController?.children)
        
    }
    
    //MARK:- TABLE VIEW
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenu.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath)as! MenuTableViewCell
        
            
        cell.lblMenu.text = arrMenu[indexPath.row]["name"]
        cell.imgMenu.image = UIImage(named: arrMenu[indexPath.row]["image"]!)
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0 :
            if seleectMenuIndex == "0"{
                self.drawerController?.closeSide()
            }else{
                self.drawController(id:"DiscoverViewController")
                seleectMenuIndex = "0"
            }
            
        case 1 :
            if seleectMenuIndex == "1"{
                self.drawerController?.closeSide()
            }else{
                self.drawController(id:"AccountSettingViewController")
                seleectMenuIndex = "1"
            }
            
        default:
            if seleectMenuIndex == "5"{
                self.drawerController?.closeSide()
            }else{
                self.drawController(id:"RegisterViewController")
                seleectMenuIndex = "5"
            }
        }
        
        
    }
    
}
