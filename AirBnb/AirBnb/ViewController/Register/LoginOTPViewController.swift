//
//  LoginOTPViewController.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 08/06/2021.
//

import UIKit
import CHIOTPField

class LoginOTPViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var miniView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var emailAddressView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var miniViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var tfOTP: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    
    //MARK:- VARIABLE
    
    var phoneCell: CHIOTPFieldTwo {
        
        return tfOTP as! CHIOTPFieldTwo
        
    }
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        miniViewConstraint.constant = 0
        mainView.isHidden = true
        addBorder()
        lblNumber.text = VariableDecleration.shared.phone_Number
        
    }
    
    
    
    //MARK:- FUNCTION
   
    
    func addBorder() {
        
        emailAddressView.layer.borderWidth = 1
        emailAddressView.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        
        passwordView.layer.borderWidth = 1
        passwordView.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        
        
    }
    
    func checkValidity(){
        
        if AppUtility!.isEmpty(self.tfOTP.text!){
                   AppUtility?.displayAlert(title: NSLocalizedString("Error", comment: ""), messageText: NSLocalizedString("Enter a valid code", comment: ""), delegate: self)
                   return
               }
    
      
       
    }
    
    func emailCheckValidity(){
        if AppUtility!.isEmpty(self.tfEmail.text!){
                   AppUtility?.displayAlert(title: NSLocalizedString("Error", comment: ""), messageText: NSLocalizedString("Enter a valid email", comment: ""), delegate: self)
                   return
               }
        if AppUtility!.isEmpty(self.tfPassword.text!){
                   AppUtility?.displayAlert(title: NSLocalizedString("Error", comment: ""), messageText: NSLocalizedString("Enter a valid password", comment: ""), delegate: self)
                   return
               }
        
    }
    
    
    //MARK:- BUTTON ACTION
    
    @IBAction func emailAccessButtonPressed(_ sender: UIButton) {
        
        let when = DispatchTime.now() + 0.3
        DispatchQueue.main.asyncAfter(deadline: when){ [self] in
            mainView.isHidden = false
            miniViewConstraint.constant = 308
        }
        
    }

    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        print(phoneCell.text!)
        checkValidity()
    }
    
    @IBAction func emailNextButtonPressed(_ sender: UIButton) {
        emailCheckValidity()
    }
    
}
