//
//  LoginPhoneNumberViewController.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 08/06/2021.
//

import UIKit

class LoginPhoneNumberViewController: UIViewController, UITextFieldDelegate {
    //MARK:- OUTLET
    
    @IBOutlet weak var tfCountryCode: UITextField!
    @IBOutlet weak var tfPhoneNumber: ATCTextField!
    
    //MARK:- DECLERATION
    
    var number: String!
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tfCountryCode.delegate = self
        tfPhoneNumber.delegate = self
        tfCountryCode.setRightPaddingPoints(5)
       
    }
    
    //MARK:- FUNCTION
    
    func checkValidity(){
        
        if AppUtility!.isEmpty(self.tfPhoneNumber.text!){
                   AppUtility?.displayAlert(title: NSLocalizedString("Error", comment: ""), messageText: NSLocalizedString("Enter a valid phone number", comment: ""), delegate: self)
                   return
               }
        
        if AppUtility!.isEmpty(self.tfCountryCode.text!){
                   AppUtility?.displayAlert(title: NSLocalizedString("Error", comment: ""), messageText: NSLocalizedString("Enter a valid country code", comment: ""), delegate: self)
                   return
               }
       
    }
    
    //MARK:- UITEXTFIELD DELEGATE
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        number = tfCountryCode.text! + tfPhoneNumber.text!
        print(number!)
        
        VariableDecleration.shared.phone_Number = number
        
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool {
           if textField == tfCountryCode {
               let maxLength = 5
               let currentString: NSString = textField.text! as NSString
               let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
               return newString.length <= maxLength
           }
        
        if textField == tfPhoneNumber {
            let maxLength = 15
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }

           return true
       }
   
    
    
    
    //MARK:- BUTTON ACTION
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        checkValidity()
        let vc = storyboard?.instantiateViewController(withIdentifier: "LoginOTPViewController")as! LoginOTPViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
