//
//  InformationViewController.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 07/05/2021.
//

import UIKit

class InformationViewController: UIViewController {

    //MARK:- OUTLET
    
    @IBOutlet weak var firstNameView: UIView!
    @IBOutlet weak var secondNameView: UIView!
    @IBOutlet weak var emailAddressView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var confirmPasswordView: UIView!
    
    
    @IBOutlet weak var tfFirstName: UITextField!
    @IBOutlet weak var tfSecondName: UITextField!
    @IBOutlet weak var tfEmailAddress: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addBorder()
        
    }
    
    
    //MARK:- FUNCTION
    
    
    func addBorder() {
        
        firstNameView.layer.borderWidth = 1
        firstNameView.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        
        secondNameView.layer.borderWidth = 1
        secondNameView.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        
        emailAddressView.layer.borderWidth = 1
        emailAddressView.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        
        passwordView.layer.borderWidth = 1
        passwordView.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        
        confirmPasswordView.layer.borderWidth = 1
        confirmPasswordView.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        
    }
    
    func checkValidity(){
        
        if AppUtility!.isEmpty(self.tfFirstName.text!){
                   AppUtility?.displayAlert(title: NSLocalizedString("Error", comment: ""), messageText: NSLocalizedString("Empty first name", comment: ""), delegate: self)
                   return
               }
        if AppUtility!.isEmpty(self.tfSecondName.text!){
                   AppUtility?.displayAlert(title: NSLocalizedString("Error", comment: ""), messageText: NSLocalizedString("Empty second name", comment: ""), delegate: self)
                   return
               }
        
        if AppUtility!.isEmpty(self.tfEmailAddress.text!){
                   AppUtility?.displayAlert(title: NSLocalizedString("Error", comment: ""), messageText: NSLocalizedString("Empty email address", comment: ""), delegate: self)
                   return
               }
         
        if AppUtility!.isEmpty(self.tfPassword.text!){
                   AppUtility?.displayAlert(title: NSLocalizedString("Error", comment: ""), messageText: NSLocalizedString("Empty password", comment: ""), delegate: self)
                   return
               }
        
        if AppUtility!.isEmpty(self.tfConfirmPassword.text!){
                   AppUtility?.displayAlert(title: NSLocalizedString("Error", comment: ""), messageText: NSLocalizedString("Empty confirm password", comment: ""), delegate: self)
                   return
               }
        
        if !AppUtility!.isEmail(self.tfEmailAddress.text!){
            AppUtility?.displayAlert(title: NSLocalizedString("Error", comment: ""), messageText: NSLocalizedString("Email is not valid", comment: ""), delegate: self)
        return
        }
        
    
        if (tfPassword.text != tfConfirmPassword.text){
        AppUtility?.displayAlert(title: NSLocalizedString("Error", comment: ""), messageText: NSLocalizedString("Your password do not match", comment: ""), delegate: self)
        return
    
        }
    
    
       
    }
    
    
    //MARK:- BUTTON ACTION
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
       
        checkValidity()
        VariableDecleration.shared.email = tfEmailAddress.text!
        let vc = storyboard?.instantiateViewController(withIdentifier: "ProfileImageViewController")as! ProfileImageViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    

    @IBAction func backButtonPressed(_ sender: UIButton) {
    
        self.navigationController?.popViewController(animated: true)
    
    }
    
}
