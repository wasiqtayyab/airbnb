//
//  RoomAvailableViewController.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 10/07/2021.
//

import UIKit

class RoomAvailableViewController: UIViewController {
    //MARK:- OUTLET
    
    
    // 1st room
    @IBOutlet weak var guestView: UIView!
    
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblMainRoom: UILabel!
    @IBOutlet weak var roomConst: NSLayoutConstraint!
    @IBOutlet weak var imgTapped: UIImageView!
    @IBOutlet weak var lblGuest: UILabel!
    @IBOutlet weak var roomView: UIView!
    
    
    @IBOutlet weak var lblLarge: UILabel!
    @IBOutlet weak var lblLarge1: UILabel!
    @IBOutlet weak var lblLarge2: UILabel!
    @IBOutlet weak var lblLarge3: UILabel!
    
    @IBOutlet weak var lblBed: UILabel!
    @IBOutlet weak var lblBed1: UILabel!
    @IBOutlet weak var lblBed2: UILabel!
    @IBOutlet weak var lblBed3: UILabel!
    
    @IBOutlet weak var lblRoom: UILabel!
    
    @IBOutlet weak var imgAvailable: UIImageView!
    @IBOutlet weak var imgAvailable1: UIImageView!
    @IBOutlet weak var imgAvailable2: UIImageView!
    @IBOutlet weak var imgAvailable3: UIImageView!
    
    
    // 2nd room
    
    @IBOutlet weak var roomView2: UIView!
    @IBOutlet weak var imgTapped1: UIImageView!
    @IBOutlet weak var lblPrice1: UILabel!
    @IBOutlet weak var roomCons1: NSLayoutConstraint!
    
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var lblRoom1: UILabel!
    @IBOutlet weak var lblMainRoom1: UILabel!
    
    @IBOutlet weak var lineCons: NSLayoutConstraint!
    
    
    // 3rd room
    
    @IBOutlet weak var roomView3: UIView!
    @IBOutlet weak var imgTapped2: UIImageView!
    @IBOutlet weak var lblPrice2: UILabel!
    
    @IBOutlet weak var roomCons2: NSLayoutConstraint!
    
    @IBOutlet weak var lineView2: UIView!
    @IBOutlet weak var lblRoom2: UILabel!
    
    @IBOutlet weak var lblMainRoom2: UILabel!
    
    
    //MARK:- VARIABLE DECLERATION
    
    var number = 0
    var isSelec1 = false
    var isSelec2 = false
    var isSelec3 = false
    
    //MARK:- VIEW DID LOAD
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblMainRoom.textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
        lblPrice.textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
        
        lblMainRoom1.textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
        lblPrice1.textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
        
        lblMainRoom2.textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
        lblPrice2.textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
        setup()
        hide()
        
        
    }
    //MARK:- FUNCTION
    
    func hide(){
        lblLarge.isHidden = true
        lblLarge1.isHidden = true
        lblLarge2.isHidden = true
        lblLarge3.isHidden = true
        
        imgAvailable.isHidden = true
        imgAvailable1.isHidden = true
        imgAvailable2.isHidden = true
        imgAvailable3.isHidden = true
        
        lblRoom.isHidden = true
        
        lblBed.isHidden = true
        lblBed1.isHidden = true
        lblBed2.isHidden = true
        lblBed3.isHidden = true
        
        lblRoom1.isHidden = true
        lineView.isHidden = true
        lineCons.constant = 0
        
        lblRoom2.isHidden = true
        lineView2.isHidden = true
        
        
    }
    
    
    func setup(){
        imgTapped.isHidden = true
        imgTapped1.isHidden = true
        imgTapped2.isHidden = true
        
        guestView.layer.borderWidth = 1.5
        guestView.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        
        roomView.layer.borderWidth = 1.5
        roomView.layer.borderColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)
        
        roomView2.layer.borderWidth = 1.5
        roomView2.layer.borderColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)
        
        roomView3.layer.borderWidth = 1.5
        roomView3.layer.borderColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.availableButtonPressed(_:)))
        roomView.addGestureRecognizer(tap)
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.availableButtonPressed1(_:)))
        roomView2.addGestureRecognizer(tap1)
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.availableButtonPressed2(_:)))
        roomView3.addGestureRecognizer(tap2)
        
        roomConst.constant = 100
        roomCons1.constant = 100
        roomCons2.constant = 100
        
        
        
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func increaseButtonPressed(_ sender: UIButton) {
        number += 1
        
        lblGuest.text = String(number)
    }
    
    @IBAction func decreaseButtonPressed(_ sender: UIButton) {
        number -= 1
        if (number < 0){
            number = 0
        }
        lblGuest.text = String(number)
    }
    
    @objc func availableButtonPressed(_ sender: UITapGestureRecognizer) {
        if isSelec1 == false {
            imgTapped.isHidden = false
            imgTapped1.isHidden = true
            imgTapped2.isHidden = true
            UIView.animate(withDuration: 0.1, animations: {
                self.roomConst.constant = 250.0
                self.roomView.layer.borderColor = #colorLiteral(red: 0.1960784314, green: 0.2470588235, blue: 0.3176470588, alpha: 1)
                self.lblMainRoom.textColor = #colorLiteral(red: 0.1960784314, green: 0.2470588235, blue: 0.3176470588, alpha: 1)
                self.lblPrice.textColor = #colorLiteral(red: 0.1960784314, green: 0.2470588235, blue: 0.3176470588, alpha: 1)
                self.lblLarge.isHidden = false
                self.lblLarge1.isHidden = false
                self.lblLarge2.isHidden = false
                self.lblLarge3.isHidden = false
                
                self.imgAvailable.isHidden = false
                self.imgAvailable1.isHidden = false
                self.imgAvailable2.isHidden = false
                self.imgAvailable3.isHidden = false
                self.lineCons.constant = 0
                self.lblRoom.isHidden = false
                
                self.lblBed.isHidden = false
                self.lblBed1.isHidden = false
                self.lblBed2.isHidden = false
                self.lblBed3.isHidden = false
                
                self.lblMainRoom1.textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
                self.lblPrice1.textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
                self.roomView2.layer.borderColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)
                self.lblRoom1.isHidden = true
                self.lineView.isHidden = true
                self.roomCons1.constant = 100
                
                self.roomCons2.constant = 100
                self.lblRoom2.isHidden = true
                self.lineView2.isHidden = true
                self.roomView3.layer.borderColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)
                self.lblPrice2.textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
                self.lblMainRoom2 .textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
                
                
            })
            
            
            
            isSelec1 = true
        }else {
            imgTapped.isHidden = true
            UIView.animate(withDuration: 0.1, animations: {
                self.roomConst.constant = 100.0
                self.roomView.layer.borderColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)
                self.lblMainRoom.textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
                self.lblPrice.textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
                self.lblLarge.isHidden = true
                self.lblLarge1.isHidden = true
                self.lblLarge2.isHidden = true
                self.lblLarge3.isHidden = true
                
                self.imgAvailable.isHidden = true
                self.imgAvailable1.isHidden = true
                self.imgAvailable2.isHidden = true
                self.imgAvailable3.isHidden = true
                
                self.lblRoom.isHidden = true
                
                self.lblBed.isHidden = true
                self.lblBed1.isHidden = true
                self.lblBed2.isHidden = true
                self.lblBed3.isHidden = true
            })
            isSelec1 = false
            
        }
        
    }
    
    @objc func availableButtonPressed1(_ sender: UITapGestureRecognizer) {
        if isSelec2 == false {
            imgTapped1.isHidden = false
            imgTapped.isHidden = true
            imgTapped2.isHidden = true
            UIView.animate(withDuration: 0.1, animations: {
                
                self.lblMainRoom1.textColor = #colorLiteral(red: 0.1960784314, green: 0.2470588235, blue: 0.3176470588, alpha: 1)
                self.lblPrice1.textColor = #colorLiteral(red: 0.1960784314, green: 0.2470588235, blue: 0.3176470588, alpha: 1)
                self.roomView2.layer.borderColor = #colorLiteral(red: 0.1960784314, green: 0.2470588235, blue: 0.3176470588, alpha: 1)
                self.lineCons.constant = 1
                self.lblRoom1.isHidden = false
                self.lineView.isHidden = false
                self.roomCons1.constant = 150
                self.roomConst.constant = 100.0
                self.roomView.layer.borderColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)
                self.lblMainRoom.textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
                self.lblPrice.textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
                self.lblLarge.isHidden = true
                self.lblLarge1.isHidden = true
                self.lblLarge2.isHidden = true
                self.lblLarge3.isHidden = true
                
                self.imgAvailable.isHidden = true
                self.imgAvailable1.isHidden = true
                self.imgAvailable2.isHidden = true
                self.imgAvailable3.isHidden = true
                
                self.lblRoom.isHidden = true
                
                self.lblBed.isHidden = true
                self.lblBed1.isHidden = true
                self.lblBed2.isHidden = true
                self.lblBed3.isHidden = true
                
                
                
                self.roomCons2.constant = 100
                self.lblRoom2.isHidden = true
                self.lineView2.isHidden = true
                self.roomView3.layer.borderColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)
                self.lblPrice2.textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
                self.lblMainRoom2 .textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
                
            })
            
            
            
            isSelec2 = true
        }else {
            imgTapped1.isHidden = true
            
            UIView.animate(withDuration: 0.1, animations: {
                self.lblMainRoom1.textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
                self.lblPrice1.textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
                self.roomView2.layer.borderColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)
                self.lblRoom1.isHidden = true
                self.lineView.isHidden = true
                self.roomCons1.constant = 100
                
            })
            isSelec2 = false
        }
        
        
        
        
    }
    
    @objc func availableButtonPressed2(_ sender: UITapGestureRecognizer) {
        if isSelec3 == false {
            imgTapped1.isHidden = true
            imgTapped.isHidden = true
            imgTapped2.isHidden = false
            UIView.animate(withDuration: 0.1, animations: {
                self.roomCons2.constant = 140
                self.lblRoom2.isHidden = false
                self.lineView2.isHidden = false
                self.roomView3.layer.borderColor = #colorLiteral(red: 0.1960784314, green: 0.2470588235, blue: 0.3176470588, alpha: 1)
                self.lblPrice2.textColor = #colorLiteral(red: 0.1960784314, green: 0.2470588235, blue: 0.3176470588, alpha: 1)
                self.lblMainRoom2 .textColor = #colorLiteral(red: 0.1960784314, green: 0.2470588235, blue: 0.3176470588, alpha: 1)
                
                
                self.roomConst.constant = 100.0
                self.roomView.layer.borderColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)
                self.lblMainRoom.textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
                self.lblPrice.textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
                self.lblLarge.isHidden = true
                self.lblLarge1.isHidden = true
                self.lblLarge2.isHidden = true
                self.lblLarge3.isHidden = true
                
                self.imgAvailable.isHidden = true
                self.imgAvailable1.isHidden = true
                self.imgAvailable2.isHidden = true
                self.imgAvailable3.isHidden = true
                
                self.lblRoom.isHidden = true
                
                self.lblBed.isHidden = true
                self.lblBed1.isHidden = true
                self.lblBed2.isHidden = true
                self.lblBed3.isHidden = true
                
                
                self.lblMainRoom1.textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
                self.lblPrice1.textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
                self.roomView2.layer.borderColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)
                self.lblRoom1.isHidden = true
                self.lineView.isHidden = true
                self.roomCons1.constant = 100
                
                
            })
            
            
            
            
            
            isSelec3 = true
        }else {
            imgTapped2.isHidden = true
            UIView.animate(withDuration: 0.1, animations: {
                self.roomCons2.constant = 100
                self.lblRoom2.isHidden = true
                self.lineView2.isHidden = true
                self.roomView3.layer.borderColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)
                self.lblPrice2.textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
                self.lblMainRoom2 .textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
                
                
                
                
            })
            
            isSelec3 = false
        }
    }
    
    
}
