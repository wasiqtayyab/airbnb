//
//  AllCurrenciesTableViewCell.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 27/05/2021.
//

import UIKit

class AllCurrenciesTableViewCell: UITableViewCell {
    //MARK:- OUTLET
    
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var currencySymbolLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
