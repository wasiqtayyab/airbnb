//
//  RecommendationViewController.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 27/05/2021.
//

import UIKit

class RecommendationViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    //MARK:-OUTLET
    
    @IBOutlet weak var swipeView: UIView!
    
    @IBOutlet weak var countryCollectionView: UICollectionView!
    
    @IBOutlet weak var inspirationCollectionView: UICollectionView!
    
    @IBOutlet weak var topPropertiesCollectionView: UICollectionView!
    
    
    @IBOutlet weak var propertyTypeCollectionView: UICollectionView!
    
    @IBOutlet weak var nearbyPropertiesCollectionView: UICollectionView!
    
    
    //MARK:- DECLERATION
    
    let countrArr = [["City":"Autralia","Price":"$0.0","Property":"( 0 property )"],
                     ["City":"Pakistan","Price":"$4500.0","Property":"( 12 properties )"],
                     ["City":"Switzerland","Price":"$0.0","Property":"( 0 property )"]]
    
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        direction()
    }
    
    //MARK:- FUNCTION
    
    func setup(){
        countryCollectionView.delegate = self
        countryCollectionView.dataSource = self
        
        inspirationCollectionView.dataSource = self
        inspirationCollectionView.delegate = self
        
        topPropertiesCollectionView.dataSource = self
        topPropertiesCollectionView.delegate = self
        
        propertyTypeCollectionView.delegate = self
        propertyTypeCollectionView.dataSource = self
        
        nearbyPropertiesCollectionView.delegate = self
        nearbyPropertiesCollectionView.dataSource = self
        
        swipeView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
    }
    
    func direction(){
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        
        swipeDown.direction = UISwipeGestureRecognizer.Direction.down
        swipeView.addGestureRecognizer(swipeDown)
        
    }
    
    //MARK:- BUTTON ACTION
    
    @objc func handleSwipes(_ gestureRecognizer : UISwipeGestureRecognizer) {
        
        self.dismiss(animated: true, completion: nil)
        
        
    }
    
    //MARK:- COLLECTION VIEW
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == countryCollectionView{
            return countrArr.count
        }else if collectionView == inspirationCollectionView {
            return 4
        }else if collectionView == topPropertiesCollectionView {
            return 2
        }else if collectionView == propertyTypeCollectionView{
            return 4
        }else {
            
            return 2
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == countryCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PopularCountriesCollectionViewCell", for: indexPath)as! PopularCountriesCollectionViewCell
            cell.contentView.isUserInteractionEnabled = false
            cell.lblPrice.text = countrArr[indexPath.row]["Price"]
            cell.lblCountry.text = countrArr[indexPath.row]["City"]
            cell.lblProperty.text = countrArr[indexPath.row]["Property"]
            return cell
        }else if collectionView == inspirationCollectionView  {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InspirationCollectionViewCell", for: indexPath)as! InspirationCollectionViewCell
            cell.imgCity.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            cell.cityView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            
            cell.cityView.layer.shadowColor = #colorLiteral(red: 0.8901960784, green: 0.8901960784, blue: 0.8901960784, alpha: 1)
            cell.cityView.layer.shadowOpacity = 1
            cell.cityView.layer.shadowOffset = .zero
            cell.cityView.layer.shadowRadius = 2
            return cell
        }else if collectionView == topPropertiesCollectionView {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TopPropertiesCollectionViewCell", for: indexPath)as! TopPropertiesCollectionViewCell
            
            cell.roomView.layer.shadowColor = #colorLiteral(red: 0.9529411765, green: 0.9529411765, blue: 0.9529411765, alpha: 1)
            cell.roomView.layer.shadowOpacity = 1
            cell.roomView.layer.shadowOffset = .zero
            cell.roomView.layer.shadowRadius = 2
            
            cell.imgRoom.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            return cell
        }
        else if collectionView == propertyTypeCollectionView {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PropertyTypeCollectionViewCell", for: indexPath)as! PropertyTypeCollectionViewCell
            cell.roomView.layer.shadowColor = #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
            cell.roomView.layer.shadowOpacity = 1
            cell.roomView.layer.shadowOffset = .zero
            cell.roomView.layer.shadowRadius = 2
            
            return cell
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NearbyPropertiesCollectionViewCell", for: indexPath)as! NearbyPropertiesCollectionViewCell
            cell.roomView.layer.shadowColor = #colorLiteral(red: 0.9529411765, green: 0.9529411765, blue: 0.9529411765, alpha: 1)
            cell.roomView.layer.shadowOpacity = 1
            cell.roomView.layer.shadowOffset = .zero
            cell.roomView.layer.shadowRadius = 2
            
            cell.imgRoom.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            
            return cell
            
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == countryCollectionView{
            let cellSize = CGSize(width: (countryCollectionView.frame.size.width / 2.58), height: 160)
            return cellSize
        }else if collectionView == inspirationCollectionView  {
            let cellSize = CGSize(width: (inspirationCollectionView.frame.size.width / 3.28), height: 126)
            return cellSize
        }else if collectionView == topPropertiesCollectionView {
            let cellSize = CGSize(width: (inspirationCollectionView.frame.size.width / 1.15), height: 268)
            return cellSize
        }else if collectionView == propertyTypeCollectionView {
            let cellSize = CGSize(width: (inspirationCollectionView.frame.size.width / 1.7), height: 140)
            return cellSize
            
        }else {
            let cellSize = CGSize(width: (inspirationCollectionView.frame.size.width / 1.15), height: 268)
            return cellSize
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        if collectionView == countryCollectionView {
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "PropertiesViewController")as! PropertiesViewController
            self.navigationController?.pushViewController(vc, animated: true)
            

            
        }else {
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "PropertiesViewController")as! PropertiesViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
}
