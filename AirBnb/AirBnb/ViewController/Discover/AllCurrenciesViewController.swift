//
//  AllCurrenciesViewController.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 27/05/2021.
//

import UIKit

class AllCurrenciesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tableView1: UITableView!
    
    //MARK:- DECLARATION
    
    let curreArr = [["Currency":"United States Dollar","Symbol":"USD - $"],
                    ["Currency":"Chinese Yuan","Symbol":"CNY - CN¥"],
                    ["Currency":"Euro","Symbol":"EUR - €"],
                    ["Currency":"Japanese Yuan","Symbol":"JPY - ¥"],
                    ["Currency":"Pound Sterling","Symbol":"GRP - £"]]
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView1.delegate = self
        tableView1.dataSource = self
        tableView1.tableFooterView = UIView()
        
    }
    
    //MARK:- TABLEVIEW
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: "AllCurrenciesTableViewCell", for: indexPath)as! AllCurrenciesTableViewCell
        cell.currencySymbolLabel.text = "USD - $"
        cell.countryLabel.text = "United States Dollar"
        
        cell.accessoryType = .checkmark
        cell.tintColor = #colorLiteral(red: 0.9058823529, green: 0.3764705882, blue: 0.3176470588, alpha: 1)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54
    }
    //MARK:- BUTTON ACTION
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
