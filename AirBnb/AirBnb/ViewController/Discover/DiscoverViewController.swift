//
//  SelectDateViewController.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 25/04/2021.
//

import UIKit
import KWDrawerController
import CalendarDateRangePickerViewController

class DiscoverViewController: UIViewController {
    
    
    //MARK:- OUTLET
    
    @IBOutlet weak var btnMenu: UIButton!
    
    @IBOutlet weak var btnMore: UIButton!
    
    @IBOutlet weak var btnCheckIn: UIButton!
    
    @IBOutlet weak var selectDateView: UIView!
    @IBOutlet weak var lblCheckIn: UILabel!
    
    @IBOutlet weak var recommendationsView: UIView!
    @IBOutlet weak var lblPlaces: UILabel!
    @IBOutlet weak var lblCheckOut: UILabel!
    @IBOutlet weak var btnPlaces: UIButton!
    @IBOutlet weak var btnCheckOut: UIButton!
    
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var locationView: UIView!
    
    @IBOutlet weak var btnCurrencies: UIButton!
    @IBOutlet weak var triangleView: UIView!
    
    //MARK:- DECLERATION
    
    var isSelected  = false
    
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        direction()
        btnCurrencies.isHidden = true
        triangleView.isHidden = true
        VariableDecleration.shared.city_Name = "Select Places"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        btnCurrencies.isHidden = true
        triangleView.isHidden = true
        
        lblPlaces.text = VariableDecleration.shared.city_Name
    }
    
    //MARK:- FUNCTION
    
    func setUpTriangle(){
        let heightWidth = triangleView.frame.size.width
        let path = CGMutablePath()
        
        path.move(to: CGPoint(x: 0, y: heightWidth))
        path.addLine(to: CGPoint(x:heightWidth/2, y: heightWidth/2))
        path.addLine(to: CGPoint(x:heightWidth, y:heightWidth))
        path.addLine(to: CGPoint(x:0, y:heightWidth))
        
        let shape = CAShapeLayer()
        shape.path = path
        shape.fillColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        triangleView.layer.insertSublayer(shape, at: 0)
    }
    
    func direction(){
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        
        swipeUp.direction = UISwipeGestureRecognizer.Direction.up
        recommendationsView.addGestureRecognizer(swipeUp)
        
        
    }
    
    
    func setup(){
        
        
        selectDateView.layer.shadowColor = #colorLiteral(red: 0.8941176471, green: 0.8941176471, blue: 0.8941176471, alpha: 1)
        selectDateView.layer.shadowOpacity = 1
        selectDateView.layer.shadowOffset = .zero
        selectDateView.layer.shadowRadius = 10
        
        locationView.layer.shadowColor = #colorLiteral(red: 0.8941176471, green: 0.8941176471, blue: 0.8941176471, alpha: 1)
        locationView.layer.shadowOpacity = 1
        locationView.layer.shadowOffset = .zero
        locationView.layer.shadowRadius = 10
        
        
        recommendationsView.layer.shadowColor = #colorLiteral(red: 0.968627451, green: 0.968627451, blue: 0.968627451, alpha: 1)
        recommendationsView.layer.shadowOpacity = 1
        recommendationsView.layer.shadowOffset = .zero
        recommendationsView.layer.shadowRadius = 10
        recommendationsView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        setUpTriangle()
        
        
    }
    
    
    
    //MARK:- BUTTON ACTION
    
    @objc func handleSwipes(_ gestureRecognizer : UISwipeGestureRecognizer) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RecommendationViewController") as! RecommendationViewController
        let nav = UINavigationController(rootViewController: vc)
        nav.isNavigationBarHidden =  true
        nav.modalPresentationStyle = .fullScreen
        self.present(nav, animated: true, completion: nil)
        
        
        
    }
    
    
    @IBAction func MenuButtonPressed(_ sender: UIButton) {
        
        self.drawerController?.setAbsolute(true, for: .left)
        self.drawerController?.openSide(.left)
        
    }
    
    @IBAction func MoreButtonPressed(_ sender: UIButton) {
        if isSelected == false {
            btnCurrencies.isHidden = false
            triangleView.isHidden = false
            isSelected = true
        }else {
            btnCurrencies.isHidden = true
            triangleView.isHidden = true
            isSelected = false
        }
        
        
    }
    
    @IBAction func currenciesButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AllCurrenciesViewController")
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    @IBAction func checkinButtonPressed(_ sender: UIButton) {
        let dateRangePickerViewController = CalendarDateRangePickerViewController(collectionViewLayout: UICollectionViewFlowLayout())
        dateRangePickerViewController.delegate = self
        dateRangePickerViewController.minimumDate = Date()
        dateRangePickerViewController.maximumDate = Calendar.current.date(byAdding: .year, value: 2, to: Date())
        dateRangePickerViewController.selectedStartDate = Date()
        dateRangePickerViewController.selectedEndDate = Calendar.current.date(byAdding: .day, value: 10, to: Date())
        let navigationController = UINavigationController(rootViewController: dateRangePickerViewController)
        self.navigationController?.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func checkoutButtonPressed(_ sender: UIButton) {
        let dateRangePickerViewController = CalendarDateRangePickerViewController(collectionViewLayout: UICollectionViewFlowLayout())
        dateRangePickerViewController.delegate = self
        dateRangePickerViewController.minimumDate = Date()
        dateRangePickerViewController.maximumDate = Calendar.current.date(byAdding: .year, value: 2, to: Date())
        dateRangePickerViewController.selectedStartDate = Date()
        dateRangePickerViewController.selectedEndDate = Calendar.current.date(byAdding: .day, value: 10, to: Date())
        let navigationController = UINavigationController(rootViewController: dateRangePickerViewController)
        self.navigationController?.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func searchplacesButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AvailableCountriesViewController")
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    @IBAction func searchButtonPressed(_ sender: UIButton) {
    }
    
    
}

    //MARK:- CalendarDateRangePickerViewControllerDelegate

extension DiscoverViewController: CalendarDateRangePickerViewControllerDelegate {
    func didTapCancel() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func didTapDoneWithDateRange(startDate: Date!, endDate: Date!) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EE, d MMM yyyy"
        lblCheckIn.text = dateFormatter.string(from: startDate)
        lblCheckOut.text = dateFormatter.string(from: endDate)
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    
    func didCancelPickingDateRange() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func didPickDateRange(startDate: Date!, endDate: Date!) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EE, d MMM yyyy"
        lblCheckIn.text = dateFormatter.string(from: startDate)
        lblCheckOut.text = dateFormatter.string(from: endDate)
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
}

