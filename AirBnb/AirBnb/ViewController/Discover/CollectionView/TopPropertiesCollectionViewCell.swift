//
//  TopPropertiesCollectionViewCell.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 31/05/2021.
//

import UIKit
import Cosmos

class TopPropertiesCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var roomView: UIView!
    @IBOutlet weak var imgRoom: UIView!
    
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblRoom: UILabel!
}
