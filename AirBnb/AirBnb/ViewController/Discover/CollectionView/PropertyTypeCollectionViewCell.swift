//
//  PropertyTypeCollectionViewCell.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 31/05/2021.
//

import UIKit

class PropertyTypeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var roomView: UIView!
    
    @IBOutlet weak var imgRoom: UIImageView!
    @IBOutlet weak var lblType: UILabel!
    
    @IBOutlet weak var lblProperty: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    
}
