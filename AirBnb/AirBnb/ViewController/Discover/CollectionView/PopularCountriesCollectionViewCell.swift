//
//  PopularCountriesCollectionViewCell.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 28/05/2021.
//

import UIKit

class PopularCountriesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgCity: UIImageView!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblProperty: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    
}
