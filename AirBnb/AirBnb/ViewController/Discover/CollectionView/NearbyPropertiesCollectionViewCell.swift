//
//  NearbyPropertiesCollectionViewCell.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 31/05/2021.
//

import UIKit
import Cosmos
class NearbyPropertiesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgRoom: UIImageView!
    @IBOutlet weak var roomView: UIView!
    
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblRoom: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    
}
