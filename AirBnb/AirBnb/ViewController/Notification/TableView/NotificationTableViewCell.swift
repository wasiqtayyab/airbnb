//
//  NotificationTableViewCell.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 30/04/2021.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    //MARK:- OUTLET
    
    @IBOutlet weak var notificationSwitch: UISwitch!
    @IBOutlet weak var lblNotification: UILabel!
    @IBOutlet weak var lblInformation: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
