//
//  NotificationViewController.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 30/04/2021.
//

import UIKit

class NotificationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tableView1: UITableView!
    
    //MARK:- DECLERATION
    
    
    
    let notiArr = [["Notification":"Product Updates","Information":"Receive messages from our platform"],
                   ["Notification":"Remainders","Information":"Receive booking remainder, pricing nottices"],
                   ["Notification":"Promotions And Tips","Information":"Receive coupons, promotions, surveys"],
                   ["Notification":"Policy And Community","Information":"Receive updates on home sharing regulations"]]
    
    
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView1.delegate = self
        tableView1.dataSource = self
        tableView1.tableFooterView = UIView()
        
    }
    
    //MARK:- FUNCTION
    
    
    
    
    
    //MARK:- BUTTON ACTION
    
    @objc func switchChanged(_: UISwitch){
        
        
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- TABLEVIEW
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notiArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell", for: indexPath)as! NotificationTableViewCell
        cell.lblInformation.text = notiArr[indexPath.row]["Information"]
        cell.lblNotification.text = notiArr[indexPath.row]["Notification"]
        
        cell.notificationSwitch.setOn(false, animated: true)
        cell.notificationSwitch.tag = indexPath.row
        cell.notificationSwitch.addTarget(self, action: #selector(self.switchChanged(_:)), for: .valueChanged)
        
        cell.notificationSwitch.onTintColor = #colorLiteral(red: 0.9803921569, green: 0.8078431373, blue: 0.8, alpha: 1)
        cell.notificationSwitch.thumbTintColor = #colorLiteral(red: 0.8941176471, green: 0.3215686275, blue: 0.2666666667, alpha: 1)
        cell.notificationSwitch.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
}
