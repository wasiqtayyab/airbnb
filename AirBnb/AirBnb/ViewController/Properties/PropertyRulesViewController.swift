//
//  PropertyRulesViewController.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 20/05/2021.
//

import UIKit

class PropertyRulesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    

    //MARK:- OUTLET
    
    @IBOutlet weak var tableView1: UITableView!
    
    //MARK:- DECLERATION
    
    var propArr = ["Check-in","Check-out","Cards accepted at this hotel","Pets","Cancellation / prepayment","Check-in","Check-in","Check-in","Check-in"]
    var disArr = ["From 14:00 hours","Until 12:00 hours","Room accepts these cards and reserves the right to temporarily hold an account prior to arrival.","Pets are not allowed","Cancellation and repayment policies vary according to accommodation type.Please check what condition may apply.","From 14:00 hours","From 14:00 hours","From 14:00 hours","From 14:00 hours"]
    
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView1.delegate = self
        tableView1.dataSource = self
      
    }
    
    //MARK:- TABLEVIEW
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return propArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PropertyRulesTableViewCell", for: indexPath)as! PropertyRulesTableViewCell
        cell.lblDisc.text = disArr[indexPath.row]
        cell.propertLabel.text = propArr[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

   
}
