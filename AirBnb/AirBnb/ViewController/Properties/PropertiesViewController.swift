//
//  PropertiesViewController.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 17/05/2021.
//

import UIKit
import GoogleMaps
import CoreLocation


class PropertiesViewController: UIViewController, CLLocationManagerDelegate,GMSMapViewDelegate, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    
    
    //MARK:- OUTLET
    
    @IBOutlet weak var collectionView1: UICollectionView!
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var lblProperty: UILabel!
    
    
    
    @IBOutlet weak var tableView2: UITableView!
    @IBOutlet weak var sortingView: UIView!
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var popConstr: NSLayoutConstraint!
    //MARK:- DECLERATION
    
    var locationManager = CLLocationManager()
    var str_la : Double!
    var str_lo : Double!
    
    var sortArr = [["sorting":"Top Picks","isSelected": "1"],
                   ["sorting":"Most Reviews","isSelected":"0"],
                   ["sorting":"Lowest Price","isSelected":"0"],
                   ["sorting":"High Price","isSelected":"0"],
                   ["sorting":"Top Rated","isSelected":"0"]]
    
    var imageArr = ["ic_tick 2","ic_tick","ic_tick","ic_tick","ic_tick"]
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        addShadow()
        Setup()
        collectionView1.delegate = self
        collectionView1.dataSource = self
        
        
    }
    
    //MARK:- FUNCTION
    
    func Setup(){
        
        tableView2.dataSource = self
        tableView2.delegate = self
        tableView2.tableFooterView = UIView()
        
        containerView.isHidden = true
        
        
    }
    
    func addShadow(){
        
        locationView.layer.shadowColor = #colorLiteral(red: 0.9568627451, green: 0.9647058824, blue: 0.9725490196, alpha: 1)
        locationView.layer.shadowOpacity = 1
        locationView.layer.shadowOffset = .zero
        locationView.layer.shadowRadius = 10
        
        
        sortingView.layer.shadowColor = #colorLiteral(red: 0.3882352941, green: 0.3882352941, blue: 0.3882352941, alpha: 1)
        sortingView.layer.shadowOpacity = 1
        sortingView.layer.shadowOffset = .zero
        sortingView.layer.shadowRadius = 10
        sortingView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
    }
    
    
    
    //MARK:- BUTTON ACTION
    
    @IBAction func sortingButtonPressed(_ sender: UIButton) {
        containerView.isHidden = false
        popConstr.constant = 330
    }
    
    @IBAction func filterButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "PropertyFilterViewController")as! PropertyFilterViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    //MARK:- GOOGLE MAP
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager.stopUpdatingLocation()
        guard let location = locations.first else {
            return
        }
        let coordinate = location.coordinate
        str_la = location.coordinate.latitude
        str_lo = location.coordinate.longitude
        let camera =    GMSCameraPosition.camera(withLatitude: str_la, longitude: str_lo, zoom: 12.0)
        
        self.mapView.camera = camera
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude)
        marker.map = mapView
        
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        print("tapped")
    }
    
    //MARK:- TABLE VIEW
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return sortArr.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SortingTableViewCell", for: indexPath)as! SortingTableViewCell
        cell.lblSorting.text = sortArr[indexPath.row]["sorting"]!
        cell.tickedImage.image = UIImage(named: imageArr[indexPath.row])
        
        if sortArr[indexPath.row]["isSelected"] == "1" {
            
            cell.tickedImage.image = UIImage (named: "ic_tick 2")
            
        }else{
            cell.tickedImage.image = UIImage (named: "ic_tick")
            
            
            
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tableView2 {
            for var i in 0..<self.sortArr.count{
                var obj = self.sortArr[i]
                obj.updateValue("0", forKey: "isSelected")
                self.sortArr.remove(at: i)
                self.sortArr.insert(obj, at: i)
            }
            var obj = self.sortArr[indexPath.row]
            obj.updateValue("1", forKey: "isSelected")
            self.sortArr.remove(at: indexPath.row)
            self.sortArr.insert(obj, at: indexPath.row)
            self.tableView2.reloadData()
            
            let cell = tableView.cellForRow(at: indexPath) as! SortingTableViewCell
            
            cell.tickedImage.image = UIImage (named: "ic_tick 2")
            
            
            let when = DispatchTime.now() + 0.5
            DispatchQueue.main.asyncAfter(deadline: when){ [self] in
                popConstr.constant = 0
                containerView.isHidden = true
                
            }
            
            
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! SortingTableViewCell
        
        cell.tickedImage.image = UIImage (named: "ic_tick")
    }
    
    //MARK:- COLLECTION VIEW
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PropertiesCollectionViewCell", for: indexPath)as! PropertiesCollectionViewCell
        
        cell.roomView.layer.shadowColor = #colorLiteral(red: 0.9529411765, green: 0.9529411765, blue: 0.9529411765, alpha: 1)
        cell.roomView.layer.shadowOpacity = 1
        cell.roomView.layer.shadowOffset = .zero
        cell.roomView.layer.shadowRadius = 2
        
        cell.imgRoom.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSize = CGSize(width: (collectionView1.frame.size.width / 1.15), height: 268)
        return cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DetailViewController")as! DetailViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    
    
}
