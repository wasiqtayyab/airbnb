//
//  PropertySurroudingsTableViewCell.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 19/05/2021.
//

import UIKit

class PropertySurroudingsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblSurroundings: UILabel!
    @IBOutlet weak var lblKm: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
