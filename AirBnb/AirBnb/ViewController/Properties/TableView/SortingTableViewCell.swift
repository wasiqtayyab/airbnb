//
//  SortingTableViewCell.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 17/05/2021.
//

import UIKit

class SortingTableViewCell: UITableViewCell {

    @IBOutlet weak var tickedImage: UIImageView!
    @IBOutlet weak var lblSorting: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
