//
//  PropertyRulesTableViewCell.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 20/05/2021.
//

import UIKit

class PropertyRulesTableViewCell: UITableViewCell {
    //MARK:- OUTLET
    @IBOutlet weak var imgProperty: UIImageView!
    @IBOutlet weak var propertLabel: UILabel!
    @IBOutlet weak var lblDisc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
