//
//  PropertiesCollectionViewCell.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 08/07/2021.
//

import UIKit
import Cosmos
class PropertiesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgRoom: UIImageView!
    
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblRupees: UILabel!
    
    @IBOutlet weak var roomView: UIView!
    @IBOutlet weak var lblRating: UILabel!
}
