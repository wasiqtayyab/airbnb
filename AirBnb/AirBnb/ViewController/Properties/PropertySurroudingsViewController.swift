//
//  PropertySurroudingsViewController.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 19/05/2021.
//

import UIKit

class PropertySurroudingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tableView1: UITableView!
    
    //MARK:- DECLERATION
    
    let propertyArr = ["What's nearby","Top attractions","Stores and transports"]
    let surrArr = ["National Pipe Museum","National Pipe Museum","National Pipe Museum","National Pipe Museum"]
    let kmArr = ["0.2 km","0.2 km","0.2 km","0.2 km"]
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView1.delegate = self
        tableView1.dataSource = self
        
    }
    
    //MARK:-TABLEVIEW
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return propertyArr.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return surrArr.count
        case 1:
            return surrArr.count
            
        default:
            return surrArr.count
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PropertySurroudingsTableViewCell", for: indexPath)as! PropertySurroudingsTableViewCell
        
        cell.lblKm.text = kmArr[indexPath.row]
        cell.lblSurroundings.text = surrArr[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
        
    }
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // header view
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 25))
        headerView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        // image view
        let imageView = UIImageView()
//        imageView.frame = CGRect.init(x: 10, y: headerView.frame.midY, width: headerView.frame.width-10, height: headerView.frame.height)
        imageView.image = UIImage(named: "ic_check_in")
        
        
        headerView.addSubview(imageView)
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 12).isActive = true
        imageView.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 13).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 13).isActive = true
        imageView.contentMode = .scaleAspectFit
        
        
        
        // label view
        
        let label = UILabel()
//        label.frame = CGRect.init(x: 35, y: headerView.frame.midY, width: headerView.frame.width-10, height: headerView.frame.height)
        
        label.font = UIFont(name:"SofiaPro-Medium",size:15.0)
        if #available(iOS 13.0, *) {
            label.textColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
        } else {
            label.textColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
        }
        
        headerView.addSubview(label)
        
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        label.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 35).isActive = true
        label.heightAnchor.constraint(equalToConstant: 18).isActive = true
        label.text = self.propertyArr[section]
        
        
        
        return headerView
        
        
        
        
    }
    
}
