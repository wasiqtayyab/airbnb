//
//  ProfileImgaeViewController.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 07/05/2021.
//

import UIKit

class ProfileImageViewController: UIViewController, UIImagePickerControllerDelegate {

    //MARK:- OUTLET
    
    
    @IBOutlet weak var imgProfile: UIImageView!
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()

        Setup()
    }
    
    //MARK:- FUNCTION
    
    func Setup(){
        
        imgProfile.layer.masksToBounds = false
        imgProfile.layer.cornerRadius = imgProfile.frame.height/2
        imgProfile.clipsToBounds = true
        
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imgProfile.isUserInteractionEnabled = true
        imgProfile.addGestureRecognizer(tapGestureRecognizer)
    }
    
    //MARK:- IMAGE PICKER DELEGATE
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        imgProfile.image = image
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    
    

    //MARK:- BUTTON ACTION
    
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        VariableDecleration.shared.profile_Image = imgProfile.image
        let vc = storyboard?.instantiateViewController(withIdentifier: "ConfirmationViewController")as! ConfirmationViewController
        self.navigationController?.pushViewController(vc, animated: true)
    
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController()
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action: UIAlertAction) in
            imagePickerController.sourceType = .camera
            self.present(imagePickerController,animated: true,completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in
            self.present(imagePickerController,animated: true,completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            
        self.present(actionSheet,animated: true,completion: nil)
    }
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    
    }
    
}
