//
//  SurroundingsTableViewCell.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 09/07/2021.
//

import UIKit

class SurroundingsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblProperty: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
