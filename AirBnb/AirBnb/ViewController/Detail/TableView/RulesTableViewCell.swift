//
//  RulesTableViewCell.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 10/07/2021.
//

import UIKit

class RulesTableViewCell: UITableViewCell {

    @IBOutlet weak var imgRules: UIImageView!
    @IBOutlet weak var lblRules: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
