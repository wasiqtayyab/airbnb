//
//  FeaturesCollectionViewCell.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 08/07/2021.
//

import UIKit

class FeaturesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var imgInfo: UIImageView!
    @IBOutlet weak var lblDescr: UILabel!
    
}
