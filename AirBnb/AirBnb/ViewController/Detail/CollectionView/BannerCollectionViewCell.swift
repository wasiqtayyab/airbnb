//
//  BannerCollectionViewCell.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 08/07/2021.
//

import UIKit

class BannerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgBanner: UIImageView!
}
