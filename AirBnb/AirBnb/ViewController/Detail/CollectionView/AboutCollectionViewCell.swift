//
//  AboutCollectionViewCell.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 09/07/2021.
//

import UIKit

class AboutCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblAbout: UILabel!
    @IBOutlet weak var imgAbout: UIImageView!
}
