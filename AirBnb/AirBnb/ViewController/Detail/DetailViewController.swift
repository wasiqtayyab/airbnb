//
//  DetailViewController.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 08/07/2021.
//

import UIKit
import CalendarDateRangePickerViewController
class DetailViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDelegate, UITableViewDataSource,CalendarDateRangePickerViewControllerDelegate {
    
    
    
    //MARK:- OUTLET
    
    @IBOutlet weak var sliderCollectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var featureCollectionView: UICollectionView!
    
    @IBOutlet weak var aboutCollectionView: UICollectionView!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var propertyTableView: UITableView!
    @IBOutlet weak var propertyHeightCon: NSLayoutConstraint!
    
    @IBOutlet weak var rulesTableView: UITableView!
    @IBOutlet weak var btnSurroundings: UIButton!
    @IBOutlet weak var btnRules: UIButton!
    
    //MARK:- VARIABLE DECLARATION
    var arrImage = ["hotel","hotel"]
    var featuArr = [["feature":"Hotel Rooms","image":"","description":"All for your needs"],
                    ["feature":"3 guest","image":"","description":"Great stays for guests"],
                    ["feature":"3 bedrooms","image":"","description":"Highly rated apartments"],
                    ["feature":"Easy Cancellation","image":"","description":"Flexible cancellation policy "]]
    
    var aboutArr = [["about":"kitchen","image":""],
                    ["about":"Washer","image":""],
                    ["about":"Sun Terrace","image":""],
                    ["about":"Laptop-friendly","image":""],
                    ["about":"Business Center","image":""],
                    ["about":"Free Parking","image":""],
                    ["about":"Restaurant","image":""],
                    ["about":"Tv","image":""],
                    ["about":"Wifi","image":""],
                    ["about":"Breakfast","image":""],
                    ["about":"First Aid","image":""]]
    
    let propertyArr = ["What's nearby"]
    let surrArr = ["National Pipe Museum","National Pipe Museum","National Pipe Museum","National Pipe Museum"]
    let kmArr = ["0.2 km","0.2 km","0.2 km","0.2 km"]
    
    
    var propArr = ["Check-in","Check-out","Cards accepted at this hotel","Pets"]
    var disArr = ["From 14:00 hours","Until 12:00 hours","Room accepts these cards and reserves the right to temporarily hold an account prior to arrival.","Pets are not allowed"]
    
    var currentindex = 0
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sliderCollectionView.delegate = self
        sliderCollectionView.dataSource = self
        featureCollectionView.delegate = self
        featureCollectionView.dataSource = self
        
        aboutCollectionView.delegate = self
        aboutCollectionView.dataSource = self
        
        
        propertyTableView.delegate = self
        propertyTableView.dataSource = self
        
        rulesTableView.delegate = self
        rulesTableView.dataSource = self
        
        
        
        
        pageControl.numberOfPages = arrImage.count
    }
    
    //MARK:- FUNCTION
    
    
    
    
    
    //MARK:- BUTTON ACTION
    
    
    
    @IBAction func viewButtonPressed(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AllReviewsViewController") as! AllReviewsViewController
    
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func selectDatesButtonPressed(_ sender: UIButton) {
//        let dateRangePickerViewController = CalendarDateRangePickerViewController(collectionViewLayout: UICollectionViewFlowLayout())
//        dateRangePickerViewController.delegate = self
//        dateRangePickerViewController.minimumDate = Date()
//        dateRangePickerViewController.maximumDate = Calendar.current.date(byAdding: .year, value: 2, to: Date())
//        dateRangePickerViewController.selectedStartDate = Date()
//        dateRangePickerViewController.selectedEndDate = Calendar.current.date(byAdding: .day, value: 10, to: Date())
//        let navigationController = UINavigationController(rootViewController: dateRangePickerViewController)
//        self.navigationController?.present(navigationController, animated: true, completion: nil)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RoomAvailableViewController") as! RoomAvailableViewController
        
        let nav = UINavigationController(rootViewController: vc)
        nav.navigationBar.isHidden = true
        nav.modalPresentationStyle = .formSheet
        self.present(nav, animated: true, completion: nil)
        
       
        
    }
    
   
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func RulesButtonPressed(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PropertyRulesViewController") as! PropertyRulesViewController
        
        let nav = UINavigationController(rootViewController: vc)
        nav.navigationBar.isHidden = true
        nav.modalPresentationStyle = .formSheet
        self.present(nav, animated: true, completion: nil)
    }
    
    @IBAction func surroundingsButtonAction(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PropertySurroudingsViewController") as! PropertySurroudingsViewController
        let nav = UINavigationController(rootViewController: vc)
        nav.navigationBar.isHidden = true
        nav.modalPresentationStyle = .formSheet
        self.present(nav, animated: true, completion: nil)
        
    }
    
    //MARK:- SCROLL THE IMAGE
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        currentindex = Int(scrollView.contentOffset.x / sliderCollectionView.frame.size.width)
        pageControl.currentPage = currentindex
    }
    
    
    //MARK:- COLLECTION VIEW
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == sliderCollectionView{
            return arrImage.count
        }else if collectionView == featureCollectionView{
            return featuArr.count
        }else {
            return aboutArr.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == sliderCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionViewCell", for: indexPath) as! BannerCollectionViewCell
            
            cell.imgBanner.image = UIImage(named: arrImage[indexPath.row])
            return cell
        }else if collectionView == featureCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeaturesCollectionViewCell", for: indexPath) as! FeaturesCollectionViewCell
            cell.lblName.text = featuArr[indexPath.row]["feature"]
            cell.lblDescr.text = featuArr[indexPath.row]["description"]
            return cell
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AboutCollectionViewCell", for: indexPath) as! AboutCollectionViewCell
            cell.lblAbout.text = aboutArr[indexPath.row]["about"]
            
            return cell
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == sliderCollectionView{
            let size = sliderCollectionView.frame.size
            return CGSize(width: size.width, height: 220)
        }else if collectionView == featureCollectionView {
            let cellSize = CGSize(width: (featureCollectionView.frame.size.width / 2), height: 60)
            return cellSize
            
        }else {
            let cellSize = CGSize(width: (aboutCollectionView.frame.size.width / 3), height: 32)
            return cellSize
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
    
    //MARK:-TABLEVIEW
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == propertyTableView {
            return 1
        }else {
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == propertyTableView{
            return surrArr.count
        }else {
            return propArr.count
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == propertyTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SurroundingsTableViewCell", for: indexPath)as! SurroundingsTableViewCell
            
            
            
            cell.lblDistance.text = kmArr[indexPath.row]
            cell.lblProperty.text = surrArr[indexPath.row]
            return cell
            
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RulesTableViewCell", for: indexPath)as! RulesTableViewCell
            
            
            
            cell.lblRules.text = propArr[indexPath.row]
            cell.lblDesc.text = disArr[indexPath.row]
            return cell
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == propertyTableView{
            return 30
        }else {
            return UITableView.automaticDimension
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == propertyTableView{
            return 30
        }else {
            return 0
        }
       
        
    }
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        
        if tableView == propertyTableView{
            return 30
        }else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // header view
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 25))
        headerView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        // image view
        let imageView = UIImageView()
        //        imageView.frame = CGRect.init(x: 10, y: headerView.frame.midY, width: headerView.frame.width-10, height: headerView.frame.height)
        imageView.image = UIImage(named: "ic_check_in")
        
        
        headerView.addSubview(imageView)
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 12).isActive = true
        imageView.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 13).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 13).isActive = true
        imageView.contentMode = .scaleAspectFit
        
        
        
        // label view
        
        let label = UILabel()
        //        label.frame = CGRect.init(x: 35, y: headerView.frame.midY, width: headerView.frame.width-10, height: headerView.frame.height)
        
        label.font = UIFont(name:"SofiaPro-Medium",size:14.0)
        if #available(iOS 13.0, *) {
            label.textColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
        } else {
            label.textColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
        }
        
        headerView.addSubview(label)
        
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        label.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 35).isActive = true
        label.heightAnchor.constraint(equalToConstant: 18).isActive = true
        label.text = self.propertyArr[section]
        
        
        
        return headerView
        
        
        
        
    }
    
    //MARK:- CalendarDateRangePickerViewControllerDelegate
    
    func didTapCancel() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func didTapDoneWithDateRange(startDate: Date!, endDate: Date!) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EE, d MMM yyyy"
        start_date = dateFormatter.string(from: startDate)
        end_date = dateFormatter.string(from: endDate)
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    
    func didCancelPickingDateRange() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func didPickDateRange(startDate: Date!, endDate: Date!) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EE, d MMM yyyy"
        start_date = dateFormatter.string(from: startDate)
        end_date = dateFormatter.string(from: endDate)
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    
}


