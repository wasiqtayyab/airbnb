//
//  AvailableCountriesTableViewCell.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 28/04/2021.
//

import UIKit

class AvailableCountriesTableViewCell: UITableViewCell {

    //MARK:- OUTLET
    
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var propertyLabel: UILabel!
    @IBOutlet weak var lblAverage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
