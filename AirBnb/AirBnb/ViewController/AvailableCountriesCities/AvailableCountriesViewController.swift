//
//  AvailableCountriesViewController.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 28/04/2021.
//

import UIKit
import KWDrawerController
class AvailableCountriesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    
    //MARK:- OUTLET

    @IBOutlet weak var tableView1: UITableView!
    
    
    
    //MARK:- DECLERATION
    
    
    let cityArr = ["Sydney","Melbourne","Perth"]
    let propertyArr = ["( 0 Property )","( 0 Property )","( 0 Property )"]
    let avgArr = ["Avg. $0.0 / Night","Avg. $0.0 / Night","Avg. $0.0 / Night"]
    let countryArr = ["Australia", "Pakistan","Canada"]
    
   
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView1.delegate = self
        tableView1.dataSource = self
    }
    
    
    //MARK:- FUNCTION
    
    
    
    
    //MARK:- BUTTON ACTION
   
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- TABLEVIEW
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return countryArr.count
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return cityArr.count
        case 1:
            return cityArr.count
            
        default:
            return cityArr.count
        }
       
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AvailableCountriesTableViewCell", for: indexPath)as! AvailableCountriesTableViewCell
        if indexPath.section == 0{
            cell.lblAverage.text = avgArr[indexPath.row]
            cell.lblCity.text = cityArr[indexPath.row]
            cell.propertyLabel.text = propertyArr[indexPath.row]
        }else  if indexPath.section == 1{
            cell.lblAverage.text = avgArr[indexPath.row]
            cell.lblCity.text = cityArr[indexPath.row]
            cell.propertyLabel.text = propertyArr[indexPath.row]
        }else  if indexPath.section == 2{
            cell.lblAverage.text = avgArr[indexPath.row]
            cell.lblCity.text = cityArr[indexPath.row]
            cell.propertyLabel.text = propertyArr[indexPath.row]
        }
       
      
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 55
        
    }
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 55
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
        
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 25))
        headerView.backgroundColor = #colorLiteral(red: 0.9568627451, green: 0.9647058824, blue: 0.9725490196, alpha: 1)
        
        let label = UILabel()

        
       
        
        label.font = UIFont(name:"SofiaPro",size:14.0)
        if #available(iOS 13.0, *) {
            label.textColor = #colorLiteral(red: 0.1764705882, green: 0.2196078431, blue: 0.2823529412, alpha: 1)
        } else {
            label.textColor = #colorLiteral(red: 0.1764705882, green: 0.2196078431, blue: 0.2823529412, alpha: 1)
        }
        
        
        let label1 = UILabel()

        
       
        
        label1.font = UIFont(name:"SofiaPro-Light",size:10.0)
        if #available(iOS 13.0, *) {
            label1.textColor = #colorLiteral(red: 0.6588235294, green: 0.6588235294, blue: 0.6588235294, alpha: 1)
        } else {
            label1.textColor = #colorLiteral(red: 0.6588235294, green: 0.6588235294, blue: 0.6588235294, alpha: 1)
        }
       
        headerView.addSubview(label)
        headerView.addSubview(label1)
        
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        label.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 10).isActive = true
        
        
        label1.translatesAutoresizingMaskIntoConstraints = false
        label1.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        label1.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -10.0).isActive = true
        
        
        
        label.text = self.countryArr[section]
        
        label1.text = "( 0 property )"
        return headerView
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! AvailableCountriesTableViewCell
        VariableDecleration.shared.city_Name = cell.lblCity.text
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
}
