//
//  AllReviewsTableViewCell.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 08/05/2021.
//

import UIKit
import Cosmos
import ReadMoreTextView

class AllReviewsTableViewCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource {
  
    //MARK:- OUTLET
    
    @IBOutlet weak var imgRoom: UIImageView!
    @IBOutlet weak var lblReview: UILabel!
    @IBOutlet weak var reviewView: UIView!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var triangleView: UIView!
    @IBOutlet weak var reviewTextView: ReadMoreTextView!
    @IBOutlet weak var collectionView1: UICollectionView!
    
    @IBOutlet weak var btnDelete: UIButton!
    //MARK:- VARIABLE
    
    
    
    
    
    
    //MARK:- NIB
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgRoom.layer.masksToBounds = false
        imgRoom.layer.cornerRadius = imgRoom.frame.height/2
        imgRoom.clipsToBounds = true
       
       setUpTriangle()
        reviewView.layer.shadowColor = #colorLiteral(red: 0.8862745098, green: 0.8862745098, blue: 0.8862745098, alpha: 1)
        reviewView.layer.shadowOpacity = 1
        reviewView.layer.shadowOffset = .zero
        reviewView.layer.shadowRadius = 10
        
        btnDelete.isHidden = true
        triangleView.isHidden = true
    }
    
    //MARK:- FUNCTION
    
    func setUpTriangle(){
            let heightWidth = triangleView.frame.size.width
               let path = CGMutablePath()

               path.move(to: CGPoint(x: 0, y: heightWidth))
               path.addLine(to: CGPoint(x:heightWidth/2, y: heightWidth/2))
               path.addLine(to: CGPoint(x:heightWidth, y:heightWidth))
               path.addLine(to: CGPoint(x:0, y:heightWidth))

               let shape = CAShapeLayer()
               shape.path = path
               shape.fillColor = #colorLiteral(red: 0.968627451, green: 0.968627451, blue: 0.968627451, alpha: 1)

               triangleView.layer.insertSublayer(shape, at: 0)
           }
    
    //MARK:- COLLECTION VIEW
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCollectionViewCell", for: indexPath) as! PhotoCollectionViewCell
        cell.roomImage.image = UIImage(named: "hotel")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 4.3 , height: 80)
    }
    
   //MARK:- BUTTON ACTION
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
   
    @IBAction func moreOptionButtonPressed(_ sender: UIButton) {
        
        if btnDelete.isHidden {
            btnDelete.isHidden = false
            triangleView.isHidden = false
            } else {
                btnDelete.isHidden = true
                triangleView.isHidden = true
            }
       
        
        
    }
    
    
    @IBAction func deleteButtonPressed(_ sender: UIButton) {
        print("tapped")
        
    }
    
  
   
   
    
}

