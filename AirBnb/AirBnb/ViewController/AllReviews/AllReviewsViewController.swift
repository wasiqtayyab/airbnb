//
//  AllReviewsViewController.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 08/05/2021.
//

import UIKit
import ReadMoreTextView

class AllReviewsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tableView1: UITableView!
    
    
    //MARK:- VARIABLE
    
    var expandedCells = Set<Int>()
   
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        
    }
   
    override func viewDidLayoutSubviews() {
        tableView1.reloadData()
    }
    
    
    
    //MARK:- FUNCTION
    
    
    
   
    
    //MARK:- BUTTON ACTION
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- TABLEVIEW
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllReviewsTableViewCell", for: indexPath)as! AllReviewsTableViewCell
        
        let reviewTextView = cell.contentView.viewWithTag(1) as! ReadMoreTextView
        reviewTextView.shouldTrim = !expandedCells.contains(indexPath.row)
        reviewTextView.setNeedsUpdateTrim()
        reviewTextView.layoutIfNeeded()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
       
        
        let reviewTextView = cell.contentView.viewWithTag(1) as! ReadMoreTextView
        reviewTextView.onSizeChange = { [unowned tableView, unowned self] r in
            let point = tableView.convert(r.bounds.origin, from: r)
            guard let indexPath = tableView.indexPathForRow(at: point) else { return }
            if r.shouldTrim {
                self.expandedCells.remove(indexPath.row)
            } else {
                self.expandedCells.insert(indexPath.row)
            }
            tableView1.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)!
        let reviewTextView = cell.contentView.viewWithTag(1) as! ReadMoreTextView
        reviewTextView.shouldTrim = !reviewTextView.shouldTrim
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    
   
   
    
}
