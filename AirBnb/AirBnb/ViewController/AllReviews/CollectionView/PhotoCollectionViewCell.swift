//
//  PhotoCollectionViewCell.swift
//  AirBnb
//
//  Created by Wasiq Tayyab on 09/05/2021.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var roomImage: UIImageView!
}
